export const timeout_XXS = { timeout: 500 }; // pul sekundy
export const timeout_XS = { timeout: 2000 }; // 2s
export const timeout_S = { timeout: 5000 }; // 5s
export const timeout_M = { timeout: 10000 }; // 10s
export const timeout_L = { timeout: 30000 }; // 30s
export const timeout_XL = { timeout: 60000 }; // 1min
export const timeout_XXL = { timeout: 180000 }; //3 min
