import BASE_URL from "../../baseUrl";

//cookies
export const adminGuided_cookieName = "AdminGuided";
export const adminGuided_cookieValue = "administrator%20took%20a%20guide";
export const alreadyGuided_cookieName = "AlreadyGuided";
export const alreadyGuided_cookieValue = "user%20took%20a%20guide";

//notifikace
export const testNotifTemplateId = "12488"; //prepinat

//export const testNotifTemplateName = "Úkoly NTF"; //prepinat
//record
export const value_string_nazevSub = "CyTest";
export const value_int_naplanovano = "123";
export const editedValue_int_naplanovano = "321";
export const value_decimal_pocetDniRealita = "7";
export const editedValue_decimal_pocetDniRealita = "14";
export const editedValue_string_vlastnik = "119997";
//dataTime
export const value_dataTime_datumPorizeni = "010120190000";
export const editedValue_dataTime_datumPorizeni = "020120190000";
//
//dyn. select
export const value_string_vlastnik = "120207";
export const viewValue_string_vlastnik = "DOCKER FLOWIO";
//static select
export const value_string_stav = "ctyri";
export const editedValue_string_stav = "tri";
export const editedValue_string_nazevSub = "CyEditedTest";

//Editace
export const templateName = "CyEditace";
export const moduleName = "CyModul";
export const tableName = "ukol";

type EnvVariables = {
  loginName: "cypress" | "admin";
  loginPassword: "cypress" | "admin";
  userName: "Administrator" | "Cypress";
  dataSourceName: "FLOWIO" | "HeG";
  mappingUsername: "flowio_docker" | "flowio_docker";
  mappingPassword: "flowio_docker" | "flowio_docker";
  templateUrl: "/overview/12482" | "/overview/5072";
  loginCookieName: "ewaLogin--1366463102" | "ewaLogin--1571791151";
  loginCookieValue:
    | "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzgzNzAzODYsImxvZ2luIjoiY3lwcmVzcyIsInVzZXJOYW1lIjoiQ3lwcmVzcyJ9.RUE_0_RgO5W2lOaffvp0_NLIps9MF68OOYP8MWwMq18"
    | "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Mzk4NTIyNDUsImxvZ2luIjoiYWRtaW4iLCJ1c2VyTmFtZSI6IkFkbWluaXN0cmF0b3IifQ.oXmfANIB1U9EuK4iV_93ZIZpS7xsdJDJ_iFqEWtDqRo";
  XSRF_TOKEN_DEV_CookieName: "XSRF-TOKEN-DEV" | " ";
  AspNetCore_Antiforgery_CookieName:
    | ".AspNetCore.Antiforgery.KXEtIQsY9sE"
    | " ";
  testNotifTitle: "FFL2100002" | "Úkol na Michala Macka 0";
  testNotifFromTemplateName: "" | "E: Notifikace";
  testNotifTemplateName: "" | "E: Notifikace NTF";
  checkMappingURL:
    | "https://flowio.popronsystems.cz/dev/api/UserAccount/CheckMapping/2006"
    | "http://localhost:5000/api/UserAccount/CheckMapping/7";
  moduleNameForEditTest: "TestCases" | "HEG Edit";
  viewValue_dataTime_datumPorizeni: "01.01.2019 0:00:00" | "01.01.2019 0:00:00";
  //"31.12.2018 23:00:00"; pres docker se prestala ubirat ta hodina

  timeShift: "2" | "1";
};
const getDevEnv = (): EnvVariables => {
  return {
    loginName: "cypress",
    loginPassword: "cypress",
    userName: "Cypress",
    dataSourceName: "FLOWIO",
    mappingUsername: "flowio_docker",
    mappingPassword: "flowio_docker",
    templateUrl: "/overview/12482",
    //pre: "/overview/7611"
    //dev: "/overview/12482"
    loginCookieName: "ewaLogin--1366463102",
    loginCookieValue:
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MzgzNzAzODYsImxvZ2luIjoiY3lwcmVzcyIsInVzZXJOYW1lIjoiQ3lwcmVzcyJ9.RUE_0_RgO5W2lOaffvp0_NLIps9MF68OOYP8MWwMq18",
    XSRF_TOKEN_DEV_CookieName: "XSRF-TOKEN-DEV",
    AspNetCore_Antiforgery_CookieName: ".AspNetCore.Antiforgery.KXEtIQsY9sE",
    testNotifTitle: "FFL2100002",
    testNotifFromTemplateName: "",
    testNotifTemplateName: "",
    checkMappingURL:
      "https://flowio.popronsystems.cz/dev/api/UserAccount/CheckMapping/2006",
    moduleNameForEditTest: "TestCases",
    viewValue_dataTime_datumPorizeni: "01.01.2019 0:00:00",
    timeShift: "2",
  };
};

const getDockerEnv = (): EnvVariables => {
  return {
    loginName: "admin",
    loginPassword: "admin",
    userName: "Administrator",
    dataSourceName: "HeG",
    mappingUsername: "flowio_docker",
    mappingPassword: "flowio_docker",
    templateUrl: "/overview/5072",
    loginCookieName: "ewaLogin--1571791151",
    loginCookieValue:
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2Mzk4NTIyNDUsImxvZ2luIjoiYWRtaW4iLCJ1c2VyTmFtZSI6IkFkbWluaXN0cmF0b3IifQ.oXmfANIB1U9EuK4iV_93ZIZpS7xsdJDJ_iFqEWtDqRo",
    XSRF_TOKEN_DEV_CookieName: " ",
    AspNetCore_Antiforgery_CookieName: " ",
    testNotifTitle: "Úkol na Michala Macka 0",
    testNotifFromTemplateName: "E: Notifikace",
    testNotifTemplateName: "E: Notifikace NTF",
    checkMappingURL: "http://localhost:5000/api/UserAccount/CheckMapping/7",
    moduleNameForEditTest: "HEG Edit",
    viewValue_dataTime_datumPorizeni: "31.12.2018 23:00:00",
    timeShift: "1",
  };
};
export const getEnv = (): EnvVariables => {
  if (BASE_URL === "https://flowio.popronsystems.cz/dev") {
    return getDevEnv();
  }
  return getDockerEnv();
};
