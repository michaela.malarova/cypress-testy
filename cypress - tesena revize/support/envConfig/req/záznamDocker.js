import * as e from "../../../support/envConfig/variables/envVar";

export const newRecord = {
  "ukol.cislo_subjektu": {
    columnName: "ukol.cislo_subjektu",
    value: null,
    unformattedValue: null,
    unmodifiedValue: null,
  },
  "subjekty2.reference_subjektu": {
    columnName: "subjekty2.reference_subjektu",
    value: null,
    unformattedValue: null,
    unmodifiedValue: null,
  },
  "ukol.naplanovano": {
    columnName: "ukol.naplanovano",
    value: 1,
    unformattedValue: null,
    unmodifiedValue: null,
  },
  "ukol.pocet_dni_realita": {
    columnName: "ukol.pocet_dni_realita",
    value: 1,
    unformattedValue: null,
    unmodifiedValue: null,
  },
  "ukol.datum_porizeni": {
    columnName: "ukol.datum_porizeni",
    value: "20.11.2021",
    unformattedValue: null,
    unmodifiedValue: null,
  },
  "ukol.vlastnik": {
    columnName: "ukol.vlastnik",
    value: "4069852",
    unformattedValue: null,
    unmodifiedValue: null,
  },
  "ukol.stav": {
    columnName: "ukol.stav",
    value: "1",
    unformattedValue: null,
    unmodifiedValue: null,
  },
  "subjekty2.nazev_subjektu": {
    columnName: "subjekty2.nazev_subjektu",
    value: e.testNotifNewRecordName,
    unformattedValue: null,
    unmodifiedValue: null,
  },
};
