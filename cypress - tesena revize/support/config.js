import baseUrl from "./baseUrl";

Cypress.config({
  baseUrl: Cypress.config("baseUrl") || baseUrl,
});
