export const loader_image = '[data-cy="loader-image"]';
export const form_row = '[data-cy="form-row"]';
export const flowio_text_input = '[data-cy~="flowio-text-input"]';
export const modal_body = '[data-cy="modal-body"]';
export const modal_content = '[data-cy="modal-content"]'; //okno
export const data_source_name = '[data-cy="data-source-name"]';
export const user_mapping_item = '[data-cy="user-mapping-item"]';
export const user_mapping_list = '[data-cy="user-mapping-list"]';
export const add_button = '[data-cy~="add-button"]';
export const clear_button = '[data-cy~="clear-button"]';
export const users_link = '[data-cy="link link-users"]';
export const users_mapping_button = '[data-cy~="user-mapping-button"]';
export const modal_header = '[data-cy="modal-header"]'; //nadpis
export const accept_button = '[data-cy~="accept-button"]';
export const table = '[data-cy="table"]';
export const flowio_password_input = '[data-cy="flowio-password-input"]';
export const row = "[data-cy~=row]";
export const modules_container = '[data-cy="modules-nav-container"]';
export const settings_link = '[data-cy="nav-settings-link"]';
export const module_dropdown = '[data-cy="module-dropdown"]';
export const edit_button = '[data-cy~="edit-button"]';
export const input_container = '[data-cy="input-container"]';
export const label_container = '[data-cy="label-container"]';
export const submit_form_button = '[data-cy~="submit-form-button"]';
export const login_input_column = '[data-cy="Login-input-column"]';
export const form = '[data-cy="form"]';
export const search_field = '[data-cy="search-field"]';
