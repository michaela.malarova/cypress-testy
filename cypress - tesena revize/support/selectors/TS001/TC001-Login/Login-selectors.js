//buttons
export const delete_button = '[data-cy~="delete-button"]';
export const accept_button = '[data-cy~="accept-button"]';
export const log_out_button = '[data-cy~="log-out-button"]';
export const registration_button = '[data-cy~="registration-button"]';

//login page
export const google_button = '[data-cy="google-button"]';
export const google_link_image = "[data-cy=google-link-image]";
export const cs_flag = "[data-cy=language-flag-cs]";
export const en_flag = "[data-cy=language-flag-en]";
export const link_to_registration_page = "[data-cy=registration-link-button]";
export const form = '[data-cy="form"]';

//registration page
export const input_container = '[data-cy="input-container"]';
export const flowio_text_input = '[data-cy="flowio-text-input"]';
export const flowio_password_input = '[data-cy="flowio-password-input"]';

//hlavní lišta
export const settings_link = '[data-cy="nav-settings-link"]';
export const account_menu = '[data-cy="accouunt-menu-link"]';
export const modules_container = '[data-cy="modules-nav-container"]';

//settings
export const users_link = '[data-cy="link link-users"]';
export const rows = '[data-cy="rows"]';

//overview template view
export const row = "[data-cy~=row]";
