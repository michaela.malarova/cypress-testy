//butons
export const delete_button = '[data-cy~="delete-button"]';
export const undo_button = '[data-cy~="undo-button"]';
export const save_button = '[data-cy="save-button"]';

//hlavni lista
export const modules_container = '[data-cy="modules-nav-container"]';
export const notification_title = '[data-cy="notification-title"]';
export const notifications_link = '[data-cy="notifications-link"]';
export const notification_link = '[data-cy="notification-link"]';
export const notifications_dropdown_menu =
  '[data-cy="notifications-dropdown-menu"]';
export const notification_row = '[data-cy="notification-row"]';
export const template_icon = '[data-cy="template-icon"]';
export const template_name = '[data-cy="template-name"]';
export const settings_image = '[data-cy="settings-image"]';
export const form = '[data-cy="form"]';

//notifikacni okno
export const search_field = '[data-cy="search-field"]';
export const modal_content = '[data-cy="modal-content"]'; //okno
export const move_right_icon = '[data-cy="move-right-icon"]';
export const move_left_icon = '[data-cy="move-left-icon"]';
export const option_list_no_search = '[data-cy="option-list-no-search"]';
export const option_list = '[data-cy="option-list"]';
export const unselected_templates = '[data-cy="unselected-templates"]';
export const selected_templates = '[data-cy="selected-templates"]';

//detail template view
export const template_detail_container =
  '[data-cy="template-detail-container"]';
