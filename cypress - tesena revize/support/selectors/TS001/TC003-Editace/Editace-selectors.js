export const flowio_text_input = '[data-cy~="flowio-text-input"]';
export const flowio_number_input = '[data-cy~="flowio-number-input"]';

export const form = '[data-cy="form"]';
export const flowio_select_input = '[data-cy~="flowio-select-input"] > select';
export const add_row_record_button = '[data-cy~="add-row-record-button"]';
export const accept_button = '[data-cy~="accept-button"]';
export const refresh_button = '[data-cy~="refresh-button"]';
export const inline_edit_button = '[data-cy~="inline-edit-button"]';
export const delete_button = '[data-cy~="delete-button"]';
export const row = "[data-cy~=row]";
export const rows = '[data-cy="rows"]';
export const new_row = '[data-cy~="row--null"]';
export const table = '[data-cy="table"]';
export const modal_body = '[data-cy="modal-body"]';
export const form_row = '[data-cy="form-row"]';
export const add_record_button = '[data-cy~="add-record-button"]';
export const add_record_form = '[data-cy="add-record-form"]';
export const modules_container = '[data-cy="modules-nav-container"]';
export const loader_image = '[data-cy="loader-image"]';
export const modal_content = '[data-cy="modal-content"]'; //okno

export const recordAttr_string_nazevSub =
  '[data-cy="subjekty5.nazev_subjektu-input-column"]';
export const recordAttr_int_naplanovano =
  '[data-cy="ukol.naplanovano-input-column"]';

export const recordAttr_decimal_pocetDniRealita =
  '[data-cy="ukol.pocet_dni_realita-input-column"]';
export const recordAttr_dataTime_datumPorizeni =
  '[data-cy="ukol.datum_porizeni-input-column"]';
export const recordAttr_string_stav_Select =
  '[data-cy="ukol.stav-input-column"]';
export const recordAttr_string_vlastnik_Select =
  '[data-cy="ukol.vlastnik-input-column"]';
export const recordAttr_string_vlastnik_span =
  '[data-cy="subjekty7.nazev_subjektu-input-column"]';
