//login
export const flowio_password_input = '[data-cy~="flowio-password-input"]';
export const flowio_text_input = '[data-cy~="flowio-text-input"]';

//search
export const search_field = '[data-cy="search-field"]';
export const refresh_button = '[data-cy~="refresh-button"]';
export const main_container = '[data-cy="main-container"]';

//waitForLoading
export const loader_image = '[data-cy="loader-image"]';

//add template
export const add_record_button = '[data-cy~="add-record-button"]';
export const modal_content = '[data-cy="modal-content"]'; //okno
export const input_container = '[data-cy="input-container"]';
export const flowio_select_input = '[data-cy~="flowio-select-input"] > select';
export const flowio_switch_input = '[data-cy~="flowio-switch-input"]';
export const submit_form_button = '[data-cy~="submit-form-button"]';
export const flowio_number_input = '[data-cy~="flowio-number-input"]';
export const ID_column = '[data-cy~="ID-input-column"]';
//goToAdministrationOf
export const settings_link = '[data-cy="nav-settings-link"]';
export const table = '[data-cy="table"]';
//goToTemplate
export const modules_container = '[data-cy="modules-nav-container"]';
export const module_dropdown = '[data-cy="module-dropdown"]';
export const dropdown_menu = '[data-cy="dropdown-menu"]';
export const template_view_nav_link = '[data-cy="template-view-nav-link"]';
//checkDataTB
export const template_builder_container =
  '[data-cy="template-builder-container"]';
export const basic_template_info = '[data-cy="basic-template-info"]';
export const form_cell = '[data-cy="form-cell"]';
export const list_item_right_value = '[data-cy="list-item-right-value"]';
export const list_item = '[data-cy="list-item"]';
//conect template
export const template_builder_chose_template =
  '[data-cy="template-builder-chose-template"]';
export const create_icon = '[data-cy="create-icon"]';
export const flowio_search_select = '[data-cy="flowio-search-select"]';
//copyTemplate
export const row = "[data-cy~=row]";
export const copy_button = '[data-cy~="copy-button"]';
//setActionsTB
export const allow_inline_edit_settings_row =
  '[data-cy="allow-inline-edit-settings-row"]';
export const allow_delete_settings_row =
  '[data-cy="allow-delete-settings-row"]';
export const allow_create_settings_row =
  '[data-cy="allow-create-settings-row"]';
export const allow_table_export_settings_row =
  '[data-cy="allow-table-export-settings-row"]';
export const allow_edit_settings_row = '[data-cy="allow-edit-settings-row"]';
//checkNavMenu
export const template_builder_nav_card =
  '[data-cy="template-builder-nav-card"]';
//delete template
export const delete_button = '[data-cy~="delete-button"]';
export const accept_button = '[data-cy~="accept-button"]';
