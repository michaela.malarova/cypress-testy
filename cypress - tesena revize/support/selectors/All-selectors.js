//obecné
export const flowio_password_input = '[data-cy="flowio-password-input"]';
export const flowio_text_input = '[data-cy~="flowio-text-input"]';
export const flowio_select_input = '[data-cy~="flowio-select-input"] > select';
export const main_container = '[data-cy="main-container"]';
export const flowio_switch_input = '[data-cy~="flowio-switch-input"]';
export const flowio_number_input = '[data-cy~="flowio-number-input"]';

//buttons
export const refresh_button = '[data-cy~="refresh-button"]';
export const delete_button = '[data-cy~="delete-button"]';
export const accept_button = '[data-cy~="accept-button"]';
export const undo_button = '[data-cy~="undo-button"]';
export const edit_button = '[data-cy~="edit-button"]';
export const submit_button = '[data-cy~="submit-button"]';
export const users_mapping_button = '[data-cy~="user-mapping-button"]';
export const add_button = '[data-cy~="add-button"]';
export const clear_button = '[data-cy~="clear-button"]';
export const submit_form_button = '[data-cy~="submit-form-button"]';
export const model_button = '[data-cy~="model-button"]';
export const user_filter_button = '[data-cy~="user-filter-button"]';
export const search_button = '[data-cy~="search-button"]';
export const save_button = '[data-cy~="save-button"]';
export const copy_button = '[data-cy~="copy-button"]';
export const more_button = '[data-cy="button more-button"]';

//template builder
export const template_builder_container =
  '[data-cy="template-builder-container"]';
export const template_builder_form = '[data-cy="template-builder-form"]';
export const template_builder_nav = '[data-cy="template-builder-nav"]';
export const template_builder_nav_card =
  '[data-cy="template-builder-nav-card"]';
export const basic_template_info = '[data-cy="basic-template-info"]';
export const form_cell = '[data-cy="form-cell"]';
export const form_cell_right_value = '[data-cy="form-cell-right-value"]';
export const select_detail_template_form =
  '[data-cy="select-detail-template-form"]';
export const allow_inline_edit_settings_row =
  '[data-cy="allow-inline-edit-settings-row"]';
export const template_action_settings_form =
  '[data-cy="template-action-settings-form"]';
export const allow_edit_settings_row = '[data-cy="allow-edit-settings-row"]';
export const allow_delete_settings_row =
  '[data-cy="allow-delete-settings-row"]';
export const allow_create_settings_row =
  '[data-cy="allow-create-settings-row"]';
export const allow_table_export_settings_row =
  '[data-cy="allow-table-export-settings-row"]';
export const flowio_search_select = '[data-cy="flowio-search-select"]';

//export const template_action_settings_form = '[data-cy="template-action-settings-form"]';
export const template_detail_actions_container =
  '[data-cy="template-detail-actions-container"]';
export const create_icon = '[data-cy="create-icon"]';
export const list_item_right_value = '[data-cy="list-item-right-value"]';
export const list_item = '[data-cy="list-item"]';
export const template_builder_chose_join_column =
  '[data-cy="template-builder-chose-join-column"]';
export const template_builder_chose_template =
  '[data-cy="template-builder-chose-template"]';

// img
export const loader_image = '[data-cy="loader-image"]';

//male okno
export const modal_content = '[data-cy="modal-content"]'; //okno
export const modal_header = '[data-cy="modal-header"]'; //nadpis
export const modal_body = '[data-cy="modal-body"]';
export const form_row = '[data-cy="form-row"]';
export const form = '[data-cy="form"]';

//mapovani uz jmenem okno
export const input_container = '[data-cy="input-container"]';
export const label_container = '[data-cy="label-container"]';
export const label = '[data-cy="label"]';

//login page
export const google_button = '[data-cy="google-button"]';
export const google_link_image = "[data-cy=google-link-image]";

export const cs_flag = "[data-cy=language-flag-cs]";
export const en_flag = "[data-cy=language-flag-en]";
export const link_to_registration_page = "[data-cy=registration-link-button]";

//registration page
export const username_field =
  '[data-cy=registration-username-field] [data-cy="input flowio-text-input"]';
export const login_field =
  '[data-cy=registration-login-field] [data-cy="input flowio-text-input"]';
export const email_field =
  '[data-cy=registration-email-field] [data-cy="input flowio-text-input"]';
export const password_field =
  '[data-cy=registration-password-field] [data-cy="input flowio-password-input"]';
export const passwordAgain_field =
  '[data-cy=registration-passwordAgain-field] [data-cy="input flowio-password-input"]';
export const registration_button = '[data-cy="button registration-button"]';

//hlavní lišta
export const account_menu = '[data-cy="accouunt-menu-link"]';
export const log_out_button = '[data-cy="button log-out-button"]';
export const modules_container = '[data-cy="modules-nav-container"]';
export const template_view_nav_link = '[data-cy="template-view-nav-link"]';
export const module_dropdown = '[data-cy="module-dropdown"]';
export const notifications_icon = '[data-cy="notifications-icon"]';
export const notifications_link = '[data-cy="notifications-link"]';
export const notification_link = '[data-cy="notification-link"]';
export const notifications_dropdown_menu =
  '[data-cy="notifications-dropdown-menu"]';
export const settings_link = '[data-cy="nav-settings-link"]';
export const notification_row = '[data-cy="notification-row"]';
export const template_icon = '[data-cy="template-icon"]';
export const template_name = '[data-cy="template-name"]';
export const notification_title = '[data-cy="notification-title"]';
//notifikacni okno
export const move_right_icon = '[data-cy="move-right-icon"]';
export const move_left_icon = '[data-cy="move-left-icon"]';
export const option_list_no_search = '[data-cy="option-list-no-search"]';
export const option_list = '[data-cy="option-list"]';
export const unselected_templates = '[data-cy="unselected-templates"]';
export const selected_templates = '[data-cy="selected-templates"]';
export const settings_image = '[data-cy="settings-image"]';
export const dropdown_menu = '[data-cy="dropdown-menu"]';

//settings
export const users_link = '[data-cy~="link-users"]';
export const templates_link = '[data-cy~="link-templates"]';

//users-mapping
export const data_source_name = '[data-cy="data-source-name"]';
export const user_mapping_item = '[data-cy="user-mapping-item"]';
export const user_mapping_list = '[data-cy="user-mapping-list"]';
export const login_input_column = '[data-cy="Login-input-column"]';

//overview template view
export const row = "[data-cy~=row]";
export const rows = '[data-cy="rows"]';
export const new_row = '[data-cy~="row--null"]';
export const add_row_record_button = '[data-cy~="add-row-record-button"]';
export const add_record_form = '[data-cy="add-record-form"]';
export const add_record_form_button = '[data-cy="add-record-form-button"]';
export const search_field = '[data-cy="search-field"]';
export const inline_edit_button = '[data-cy~="inline-edit-button"]';
export const add_record_button = '[data-cy~="add-record-button"]';
export const table = '[data-cy="table"]';
export const table_data_item_header = '[data-cy="table-data-item-header"]';

//detail template view
export const template_detail_container =
  '[data-cy="template-detail-container"]';
//item template
export const item_template_container = '[data-cy="item-template-container"]';

//records
export const recordAttr_string_nazevSub =
  '[data-cy="subjekty5.nazev_subjektu-input-column"]';
export const recordAttr_int_naplanovano =
  '[data-cy="ukol.naplanovano-input-column"]';
export const recordAttr_decimal_pocetDniRealita =
  '[data-cy="ukol.pocet_dni_realita-input-column"]';
export const recordAttr_dataTime_datumPorizeni =
  '[data-cy="ukol.datum_porizeni-input-column"]';
export const recordAttr_string_vlastnik_Select =
  '[data-cy="ukol.vlastnik-input-column"]';
export const recordAttr_string_stav_Select =
  '[data-cy="ukol.stav-input-column"]';
export const recordAttr_reference_subjektu =
  '[data-cy="subjekty5.reference_subjektu-input-column"]';
export const recordAttr_login_id =
  '[data-cy="zamestnanci14.login_id-input-column"]';
export const recordAttr_dateTime =
  '[data-cy="ukol.datum_porizeni-input-column"]';

//kalendar
export const calendar = '[data-cy="calendar"]';
