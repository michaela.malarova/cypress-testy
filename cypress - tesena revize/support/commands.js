// https://on.cypress.io/custom-commands
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
import { any } from "bluebird";
import * as s from "./selectors/Commands-selectors";
import * as e from "./envConfig/variables/envVar";
import * as t from "./envConfig/variables/timeouts";

Cypress.Commands.add("login", (username, password) => {
  //Pole pro uz. jmeno
  cy.get('[data-cy="form"]', { timeout: 30000 }).within(() => {
    cy.get(s.flowio_text_input, { timeout: 20000 })
      .clear({ force: true })
      .then((e) => {
        if (username !== "") cy.wrap(e).type(username);
      });

    //Pole heslo
    cy.get(s.flowio_password_input, { timeout: 20000 })
      .clear({ force: true })
      .then((e) => {
        if (password !== "") cy.wrap(e).type(password);
      });
    //Přihlášení
    cy.get('[data-cy~="accept-button"]', {
      timeout: 20000,
    }).click();
  });
});
Cypress.Commands.add("search", (key) => {
  cy.get(s.search_field, { timeout: 5000 }).within(() => {
    cy.get(s.flowio_text_input, { timeout: 5000 }).clear().type(key);
  });
  cy.get(s.refresh_button).click({ force: true });
  cy.get(s.main_container).within(() => {
    cy.get(s.loader_image, { timeout: 60000 }).should("not.exist");
  });
});
Cypress.Commands.add("waitForLoading", (area) => {
  cy.wait(500);
  cy.get(area, { timeout: 30000 }).within(() => {
    cy.get(s.loader_image, { timeout: 60000 }).should("not.exist");
  });
});
Cypress.Commands.add(
  "getAttrValue",
  {
    prevSubject: any,
  },
  (prevSub, attrName) => {
    let attrValue;
    prevSub.invoke("attr", attrName).then((val) => {
      attrValue = val;
    });

    return attrValue;
  }
);
Cypress.Commands.add(
  "addTemplate",
  (templateName, templateType, moduleName) => {
    cy.get(s.add_record_button).click();
    cy.contains(s.modal_content, "Přidat šablonu", t.timeout_M).within(() => {
      cy.contains(s.input_container, "Název").within(() => {
        cy.get(s.flowio_text_input).type(templateName);
      });
      cy.contains(s.input_container, "Typ").within(() => {
        cy.get(s.flowio_select_input).select(templateType);
      });
      cy.contains(s.input_container, "Systém").within(() => {
        cy.get(s.flowio_select_input).select("HeG");
      });
      cy.contains(s.input_container, "Datový zdroj").within(() => {
        cy.get(s.flowio_select_input).select(e.getEnv().dataSourceName);
      });
      cy.contains(s.input_container, "Tabulka").within(() => {
        cy.get(s.flowio_select_input).select(e.tableName);
      });
      cy.contains(s.input_container, "Modul").within(() => {
        cy.get(s.flowio_select_input).select(moduleName);
      });
      cy.get(s.submit_form_button).click();
    });
  }
);
Cypress.Commands.add("goToAdministrationOf", (selector) => {
  cy.get(s.settings_link, t.timeout_L).click();
  cy.get(selector, t.timeout_M).click();
  cy.url().should("contain", "/administration", t.timeout_M);
  cy.get(s.table, t.timeout_L).should("be.visible");
});
Cypress.Commands.add("goToTemplate", (moduleName, templateName) => {
  cy.get(s.modules_container, t.timeout_L).within(() => {
    cy.contains(s.module_dropdown, moduleName, t.timeout_L).click();
    cy.get(s.dropdown_menu).within(() => {
      cy.contains(s.template_view_nav_link, templateName, t.timeout_S).click();
    });
  });
});
Cypress.Commands.add(
  "checkDataTB",
  (templateName, templateType, DSname, tableName) => {
    cy.contains(
      s.template_builder_container,
      templateName,
      t.timeout_XL
    ).within(() => {
      cy.get(s.basic_template_info, t.timeout_XL).within(() => {
        cy.contains(s.form_cell, "Název").within(() => {
          cy.get(s.flowio_text_input).should("have.value", templateName);
        });
        if (templateType === "Přehled") {
          cy.contains(s.form_cell, "Viditelná").within(() => {
            cy.get(s.flowio_switch_input).within(() => {
              cy.get("input").should("have.attr", "checked");
            });
          });
        } else {
          cy.contains(s.form_cell, "Viditelná").within(() => {
            cy.get(s.flowio_switch_input).within(() => {
              cy.get("input").should("not.have.attr", "checked");
            });
          });
        }

        cy.contains(s.form_cell, "Typ").within(() => {
          cy.get(s.list_item_right_value).should("have.text", templateType);
        });
        cy.contains(s.list_item, "Tabulka").within(() => {
          cy.get(s.list_item_right_value).should("have.text", tableName);
        });
        cy.contains(s.list_item, "Datový zdroj").within(() => {
          cy.get(s.list_item_right_value).should("have.text", DSname);
        });
      });
    });
  }
);
Cypress.Commands.add("templateConnection", (templateType, templateName) => {
  if (templateType == "Položková") {
    cy.contains("Přidat šablonu", t.timeout_XL).click();
  }
  cy.get(s.template_builder_chose_template, t.timeout_L).within(() => {
    cy.get(s.create_icon, t.timeout_L).should("be.visible");
    cy.get(s.flowio_search_select)
      .type(templateName)
      .type("{downarrow}")
      .type("{enter}");
  });
});
Cypress.Commands.add("copyTemplate", (oldTemplateName, newTemplateName) => {
  cy.get(s.search_field, { timeout: 5000 }).within(() => {
    cy.get(s.flowio_text_input, { timeout: 5000 })
      .clear()
      .type(oldTemplateName);
  });
  cy.get(s.main_container).within(() => {
    cy.get(s.loader_image, { timeout: 60000 }).should("not.exist");
  });
  cy.wait(500);
  cy.contains(s.row, oldTemplateName, t.timeout_XL).within(() => {
    cy.get(s.copy_button).click();
  });
  cy.contains(
    s.modal_content,
    `Kopírovat záznam - ${oldTemplateName}`,
    t.timeout_XL
  ).within(() => {
    cy.contains(s.input_container, "Název").within(() => {
      cy.get(s.flowio_text_input).clear().type(newTemplateName);
    });
    cy.get(s.submit_form_button).click();
  });
});
Cypress.Commands.add("checkNavMenuTB", (templateName) => {
  cy.contains(s.template_builder_container, templateName, t.timeout_XL).within(
    () => {
      cy.contains(s.template_builder_nav_card, "Nastavení").should(
        "be.visible"
      );
      cy.contains(s.template_builder_nav_card, "Datový model").should(
        "be.visible"
      );
      cy.contains(s.template_builder_nav_card, "Prezentační model").should(
        "be.visible"
      );
      cy.contains(s.template_builder_nav_card, "Funkce").should("be.visible");
      cy.contains(s.template_builder_nav_card, "Agregační skupiny").should(
        "be.visible"
      );
      cy.contains(s.template_builder_nav_card, "Zobrazit model").should(
        "be.visible"
      );
      cy.contains(s.template_builder_nav_card, "Náhled SQL dotazu").should(
        "be.visible"
      );
    }
  );
});
Cypress.Commands.add("setActionsTB", () => {
  cy.get(s.template_builder_container).within(() => {
    cy.contains(
      "Při zapnuté editaci nebo vytváření a editaci musí mít přiřazený detail"
    ).should("not.exist");
    cy.get(s.allow_inline_edit_settings_row).within(() => {
      cy.get(s.flowio_switch_input).click();
    });
    cy.contains(
      "Při zapnuté editaci nebo vytváření a editaci musí mít přiřazený detail"
    ).should("not.exist");
    cy.get(s.allow_delete_settings_row).within(() => {
      cy.get(s.flowio_switch_input).click();
    });
    cy.contains(
      "Při zapnuté editaci nebo vytváření a editaci musí mít přiřazený detail"
    ).should("not.exist");
    cy.get(s.allow_create_settings_row).within(() => {
      cy.get(s.flowio_switch_input).click();
    });
    cy.contains(
      "Při zapnuté editaci nebo vytváření a editaci musí mít přiřazený detail"
    ).should("not.exist");
    cy.get(s.allow_table_export_settings_row).within(() => {
      cy.get(s.flowio_switch_input).click();
    });
    cy.contains(
      "Při zapnuté editaci nebo vytváření a editaci musí mít přiřazený detail"
    ).should("not.exist");
    cy.get(s.allow_edit_settings_row).within(() => {
      cy.get(s.flowio_switch_input).click();
    });
    cy.contains(
      "Při zapnuté editaci nebo vytváření a editaci musí mít přiřazený detail"
    ).should("be.visible");
  });
  Cypress.Commands.add("deleteTemplate", (templateName) => {
    cy.contains(s.row, templateName, t.timeout_L).within(() => {
      cy.get(s.delete_button).click();
    });
    cy.wait(500);
    cy.get(s.modal_content).within(() => {
      cy.get(s.accept_button).click();
    });
    cy.get(s.modal_content, t.timeout_L).should("not.exist");
  });

  Cypress.Commands.add("shouldBeEmpty", (area) => {
    cy.get(area).within(() => {
      cy.get("input").invoke("attr", "value").should("be.empty");
    });
  });
});
