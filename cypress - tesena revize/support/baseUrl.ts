export const URL_DICT = {
  prerelease: "https://flowio.popronsystems.cz/prerelease/",
  dev: "https://flowio.popronsystems.cz/dev",
  local: "http://localhost:5555/",
};

export default URL_DICT.local;
