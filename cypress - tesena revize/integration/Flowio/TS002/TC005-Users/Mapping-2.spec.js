/// <reference types="cypress" />
import * as s from "../../../../support/selectors/TS002/TC005-Uzivatele/Mapovani-selectors";
import * as e from "../../../../support/envConfig/variables/envVar";
import * as t from "../../../../support/envConfig/variables/timeouts";
import { exit } from "process";

Cypress.Cookies.defaults({
  preserve: [
    e.getEnv().loginCookieName,
    e.adminGuided_cookieName,
    e.alreadyGuided_cookieName,
    e.getEnv().XSRF_TOKEN_DEV_CookieName,
    e.getEnv().AspNetCore_Antiforgery_CookieName,
  ],
});

context("Mapování - 2", () => {
  beforeEach(() => {
    cy.viewport(1500, 1000);
  });
  before(() => {
    cy.clearCookies({ domain: null });
    cy.setCookie(e.adminGuided_cookieName, e.adminGuided_cookieValue); //-to co se me pta na tutorial adminas
    cy.setCookie(e.alreadyGuided_cookieName, e.alreadyGuided_cookieValue);
  });

  it("Přihlášení - FLOWIO.", () => {
    cy.visit("/");
    cy.url().should("contain", `${Cypress.config().baseUrl}`);
    cy.get(s.modules_container, t.timeout_L).should("be.visible");
  });
  it("Přechod do nastavení uzivatelů.", () => {
    cy.get(s.settings_link, t.timeout_L).click();
    cy.get(s.users_link, t.timeout_L).click();
    cy.get(s.table, t.timeout_XL).should("be.visible");
  });
  it("Namapovani jen jmenem - zadaní špatného jména.", () => {
    var attrValue;
    cy.contains(s.row, e.getEnv().userName).within(() => {
      cy.get(s.users_mapping_button).click();
    });
    cy.contains(
      s.modal_content,
      `Mapování datových zdrojů pro uživatele ${e.getEnv().loginName}`
    ).within(() => {
      cy.get(s.user_mapping_list, t.timeout_XL).within(() => {
        cy.contains(s.user_mapping_item, e.getEnv().dataSourceName)
          .within(() => {
            cy.get('[data-cy~="button"]')
              .invoke("attr", "data-cy")
              .then((val) => {
                attrValue = val;
              });
          })
          .then((body) => {
            if (attrValue == "button clear-button") {
              cy.contains(
                s.user_mapping_item,
                e.getEnv().dataSourceName
              ).within(() => {
                cy.get(s.clear_button).click(); //tady je ted chyba, nejde ihned namapovat po odmapovani
              });
            } else {
            }
          });
      });
      //tady jdu obchazet chybu ve ktere nejde namapovat hned po odmapovani!!
      cy.contains("Zavřít").click();
    });

    cy.contains(s.row, e.getEnv().userName, t.timeout_XL).within(() => {
      cy.get(s.users_mapping_button).click();
    });

    cy.contains(
      s.modal_content,
      `Mapování datových zdrojů pro uživatele ${e.getEnv().loginName}`
    ).within(() => {
      cy.get(s.user_mapping_list, t.timeout_L).within(() => {
        cy.contains(s.user_mapping_item, e.getEnv().dataSourceName).within(
          () => {
            cy.get(s.add_button).click();
          }
        );
      });
    });
    // tady konci obchazeni chyby :D
    cy.contains(
      s.modal_content,
      `Datový zdroj ${e.getEnv().dataSourceName}`,
      t.timeout_L
    ).within(() => {
      cy.get(s.input_container, t.timeout_S).within(() => {
        cy.get(s.label_container).should("have.text", "HeG uživatelské jméno:");
        cy.get(s.flowio_text_input).type("wrongUserName");
      });
      cy.get(s.submit_form_button).click();
    });
    cy.contains(
      s.user_mapping_item,
      e.getEnv().dataSourceName,
      t.timeout_L
    ).within(() => {
      cy.get(s.clear_button, t.timeout_L).should("be.visible");
    });
    cy.contains("Zavřít").click();
  });
  it("Přechod na šablonu se špatným jménem - nejde.", () => {
    cy.goToTemplate(e.moduleName, e.templateName);
    cy.contains(s.modal_content, "Mapování", t.timeout_L).within(() => {
      cy.get(s.edit_button).should("be.visible"); //ZMENIT MAPOVANI
    });
    cy.get(s.table, t.timeout_L).should("not.exist");
  });
  it("Okno mapovani: namapovano na spatne jmeno + prazdne heslo - nenamapuji se", () => {
    cy.contains(s.modal_content, "Mapování", t.timeout_XL).within(() => {
      cy.contains(s.form_row, "Uživatelské jméno").within(() => {
        cy.get(s.flowio_text_input)
          .should("have.value", "wrongUserName")
          .invoke("attr", "disabled")
          .should("exist");
      });
      cy.contains(s.form_row, "Heslo").within(() => {
        cy.get(s.flowio_password_input).should("have.value", "");
      });
      cy.get(s.edit_button).should("be.visible");
      //spatne jmeno + zadne heslo
      cy.get(s.accept_button).click();
      cy.waitForLoading(s.accept_button);
    });
    cy.get(s.table).should("not.exist");
  });
  it("Okno mapovani: namapovano na spatne jmeno + spatne heslo - nenamapuji se", () => {
    cy.get(s.modal_content, t.timeout_XL).within(() => {
      cy.contains(s.form_row, "Heslo").within(() => {
        //obchazim chybu tim click (v prvni moment se field jevi jako read only)
        cy.get(s.flowio_password_input).click().type("wrongPassword");
      });
      cy.get(s.accept_button).click();
      cy.waitForLoading(s.accept_button);
    });
    cy.get(s.table).should("not.exist");
  });
  it("Okno mapovani - jen zavřu okno - nezobrazuje se sablona", () => {
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.get(".close").click();
    });
    cy.contains(s.modal_content, "Mapování", t.timeout_S).should("not.exist");
    cy.get(s.table).should("not.exist");
  });
  it("Test ZMENIT MAPOVANI - vyprazdni se jmeno + uzivatel se ze jmena odmapuje", () => {
    cy.wait(500);
    cy.goToTemplate(e.moduleName, e.templateName);
    cy.get(s.table, t.timeout_L).should("not.exist");
    cy.contains(s.modal_content, "Mapování", t.timeout_L).within(() => {
      cy.contains(s.form_row, "Uživatelské jméno").within(() => {
        cy.get(s.flowio_text_input)
          .should("have.value", "wrongUserName")
          .invoke("attr", "disabled")
          .should("exist");
      });
      cy.get(s.edit_button).click(); //ZMENIT MAPOVANI
      cy.contains(s.form_row, "Uživatelské jméno").within(() => {
        cy.get(s.flowio_text_input)
          .should("have.value", "")
          .invoke("attr", "disabled")
          .should("not.exist");
      });
      cy.get(".close").click();
    });
  });
  it("Přechod do nastavení uživatelů.", () => {
    cy.get(s.settings_link, t.timeout_L).wait(2000).click(); //ceka se na zmizeni toastu - toast cypress nevidí!
    cy.get(s.users_link, t.timeout_L).click();
    cy.get(s.table, t.timeout_XL).should("be.visible");
  });
  it("UZ je odmapovany + namapovani jmenem - zadaní spravneho jména.", () => {
    cy.contains(s.row, e.getEnv().userName, t.timeout_XL).within(() => {
      cy.get(s.users_mapping_button).should("be.visible").click();
    });
    cy.contains(
      s.modal_content,
      `Mapování datových zdrojů pro uživatele ${e.getEnv().loginName}`,
      t.timeout_L
    ).within(() => {
      cy.get(s.user_mapping_list, t.timeout_L).within(() => {
        cy.contains(
          s.user_mapping_item,
          e.getEnv().dataSourceName,
          t.timeout_L
        ).within(() => {
          cy.wait(500);
          cy.get(s.add_button).click(); //kuli zmene mapovani neni vubec namapovany
        });
      });
    });
    cy.get(s.input_container).within(() => {
      cy.get(s.label_container).should("have.text", "HeG uživatelské jméno:");
      cy.get(s.flowio_text_input).type(e.getEnv().mappingUsername);
    });
    cy.get(s.submit_form_button).click();
    cy.contains(
      s.user_mapping_item,
      e.getEnv().dataSourceName,
      t.timeout_M
    ).within(() => {
      cy.get(s.clear_button).should("be.visible");
    });
    cy.contains("Zavřít").click();
  });
  it("Přechod na šablonu s namapováním přes správné uživatelské jméno - sablona stale není viditelná.", () => {
    cy.goToTemplate(e.moduleName, e.templateName);
    cy.get(s.table).should("not.exist");
  });
  it("Zobrazi se okno mapovani + jmeno je predvyplnene a needitovatelne + zobrazuje se tlačítko ZMENIT MAPOVANI.", () => {
    cy.get(s.modal_content, t.timeout_XL).within(() => {
      cy.get(s.modal_header).contains("Mapování");
      cy.get(s.edit_button).should("be.visible").contains("Změnit mapování");
      cy.contains(s.form_row, "Uživatelské jméno").within(() => {
        cy.get(s.flowio_text_input)
          .invoke("attr", "value")
          .should("eq", e.getEnv().mappingUsername);
        cy.get(s.flowio_text_input).invoke("attr", "disabled").should("exist");
      });
    });
  });
  it("Doplním špatné heslo - nenamapuje se, šablona se nezobrazí", () => {
    cy.get(s.modal_content).within(() => {
      cy.contains(s.form_row, "Heslo").within(() => {
        //ten click obchazi chybu
        cy.get(s.flowio_password_input).click().type("wrongPassword");
      });
      cy.get(s.accept_button).click();
      cy.waitForLoading(s.accept_button);
    });
    cy.get(s.table).should("not.exist");
  });
  it("Doplním správné heslo - šablona je viditelná.", () => {
    cy.get(s.modal_content).within(() => {
      cy.contains(s.form_row, "Heslo").within(() => {
        cy.get(s.flowio_password_input)
          .clear({ force: true })
          .type(e.getEnv().mappingPassword);
      });
      cy.get(s.accept_button).click();
      cy.waitForLoading(s.accept_button);
    });
    //cy.get(s.loader_image, { timeout: 60000 }).should("not.exist");
    cy.get(s.table, t.timeout_XL).should("be.visible");
  });
});
