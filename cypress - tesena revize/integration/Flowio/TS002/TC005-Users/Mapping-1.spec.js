/// <reference types="cypress" />
import * as s from "../../../../support/selectors/TS002/TC005-Uzivatele/Mapovani-selectors";
import * as e from "../../../../support/envConfig/variables/envVar";
import * as t from "../../../../support/envConfig/variables/timeouts";

Cypress.Cookies.defaults({
  preserve: [
    e.getEnv().loginCookieName,
    e.adminGuided_cookieName,
    e.alreadyGuided_cookieName,
    e.getEnv().XSRF_TOKEN_DEV_CookieName,
    e.getEnv().AspNetCore_Antiforgery_CookieName,
  ],
});

context("Mapování - 1", () => {
  before(() => {
    cy.clearCookies({ domain: null });
    cy.setCookie(e.adminGuided_cookieName, e.adminGuided_cookieValue); //-to co se me pta na tutorial adminas
    cy.setCookie(e.alreadyGuided_cookieName, e.alreadyGuided_cookieValue);
  });

  it("Přihlášení - FLOWIO.", () => {
    cy.visit("/");
    cy.url().should("contain", `${Cypress.config().baseUrl}`);
    cy.get(s.modules_container, t.timeout_L).should("be.visible");
  });
  it("Přechod do nastavení.", () => {
    //nejde tam přejít rovnou pres URL - docker!!!
    cy.get(s.settings_link, t.timeout_L).click();
    cy.get(s.users_link, t.timeout_L).click();
    cy.get(s.table, t.timeout_XL).should("be.visible");
  });
  it("Odmapování.", () => {
    let test;
    cy.get(s.search_field).type(e.getEnv().userName);
    cy.wait(1000);
    cy.contains(s.row, e.getEnv().userName, t.timeout_L).within(() => {
      cy.get(s.users_mapping_button).click();
    });
    cy.contains(
      s.modal_content,
      `Mapování datových zdrojů pro uživatele ${e.getEnv().loginName}`,
      t.timeout_L
    ).within(() => {
      cy.get(s.user_mapping_list, t.timeout_S).within(() => {
        cy.contains(s.user_mapping_item, e.getEnv().dataSourceName)
          .within(() => {
            cy.get('[data-cy~="button"]')
              .invoke("attr", "data-cy")
              .then((val) => {
                //rada bych to mela jako fci ale nedari se mi udelat child command
                test = val;
              });
          })
          .then((body) => {
            if (test == "button clear-button") {
              cy.contains(
                s.user_mapping_item,
                e.getEnv().dataSourceName,
                t.timeout_L
              ).within(() => {
                cy.get(s.clear_button).click();
              });
            } else {
            }
          });
      });
    });
    cy.contains("Zavřít").click();
  });
  it("Přechod na šablonu bez namapování - nejde + otevře se okno pro mapování.", () => {
    cy.goToTemplate(e.moduleName, e.templateName);
    cy.get(s.modal_content, t.timeout_L).within(() => {
      cy.get(s.modal_header, t.timeout_S).contains("Mapování");
    });
    cy.get(s.table, t.timeout_L).should("not.exist");
  });
  it("Pozaduje se jmeno a heslo, nic neni predvyplnene.", () => {
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.contains(s.form_row, "Uživatelské jméno").within(() => {
        cy.get(s.flowio_text_input).should("have.value", "");
      });
      cy.contains(s.form_row, "Heslo").within(() => {
        cy.get(s.flowio_password_input).should("have.value", "");
      });
    });
  });
  it("Tlacitko ZMENIT MAPOVANI není videt.", () => {
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.get(s.edit_button).should("not.exist");
    });
  });
  it("Kontrola nazvu zdroje v okně pro mapování.", () => {
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.get(s.modal_header).contains(
        `Mapování uživatele na ${e.getEnv().dataSourceName}`
      );
    });
  });
  it("Bez spravneho vyplneni okna se sablona nezobrazi.", () => {
    //nevyplnim nic
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.get(s.accept_button).within(() => {
        cy.get(s.loader_image, t.timeout_L).should("not.exist");
      });
    });
    cy.get(s.table).should("not.exist");

    //vyplnim vse spatne
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.login("wrongUsername", "wrongPassword");
      cy.waitForLoading(s.accept_button);
    });

    //cy.get(s.table).should("not.exist"); obchazeni chyby
  });
  it("Bez spravneho vyplneni jmena se sablona nezobrazi.", () => {
    //nevyplnim jmeno
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.login("", e.getEnv().mappingPassword);
      cy.get(s.accept_button).within(() => {
        cy.get(s.loader_image, t.timeout_L).should("not.exist");
      });
    });
    cy.get(s.table).should("not.exist");

    //spatne vyplnim jmeno
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.login("wrongUsername", e.getEnv().mappingPassword);
      cy.waitForLoading(s.accept_button);
    });
    cy.get(s.table).should("not.exist");
  });
  it("Bez spravneho vyplneni hesla se sablona nezobrazi.", () => {
    //nevyplnim heslo
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.login(e.getEnv().mappingUsername, "");
      cy.get(s.accept_button).within(() => {
        cy.get(s.loader_image, t.timeout_L).should("not.exist");
      });
    });
    cy.get(s.table).should("not.exist");

    //spatne vyplnim heslo
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.login(e.getEnv().mappingUsername, "wrongPassword");
      cy.waitForLoading(s.accept_button);
    });
    cy.get(s.table).should("not.exist");
  });
  it("Okno mapovani - jen zavřu okno - nezobrazuje se sablona", () => {
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.get(".close").click();
    });
    cy.contains(s.modal_content, "Mapování", t.timeout_S).should("not.exist");
    cy.get(s.table).should("not.exist");
  });
  it("Přechod na šablonu bez namapování - nejde + otevře se okno pro mapování.", () => {
    cy.reload();
    cy.get(s.modules_container, t.timeout_L).within(() => {
      cy.contains(s.module_dropdown, e.moduleName, t.timeout_L).click();
      cy.contains(e.templateName, t.timeout_S).click();
    });
    cy.get(s.modal_content, t.timeout_L).within(() => {
      cy.get(s.modal_header, t.timeout_S).contains("Mapování");
    });
    cy.get(s.table, t.timeout_L).should("not.exist");
  });
  it("Nastavit správně mapování -> šablona je viditelná", () => {
    cy.contains(s.modal_content, "Mapování").within(() => {
      cy.login(e.getEnv().mappingUsername, e.getEnv().mappingPassword);
    });
    cy.get(s.table, t.timeout_XL).should("be.visible");
  });
});
