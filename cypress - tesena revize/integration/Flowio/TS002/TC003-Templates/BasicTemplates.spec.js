/// <reference types="cypress" />
import * as s from "../../../../support/selectors/All-selectors";
import * as e from "../../../../support/envConfig/variables/envVar";
import * as t from "../../../../support/envConfig/variables/timeouts";

Cypress.Cookies.defaults({
  preserve: [
    e.getEnv().loginCookieName,
    e.adminGuided_cookieName,
    e.alreadyGuided_cookieName,
    e.getEnv().XSRF_TOKEN_DEV_CookieName,
    e.getEnv().AspNetCore_Antiforgery_CookieName,
  ],
});
context("Test základních šablon.", () => {
  before(() => {
    cy.clearCookies({ domain: null });
    cy.setCookie(e.adminGuided_cookieName, e.adminGuided_cookieValue);
    cy.setCookie(e.alreadyGuided_cookieName, e.alreadyGuided_cookieValue);
  });
  it("Přechod do FLOWIA", () => {
    //nic uz s tim proboha nedelej
    cy.visit("/");
    cy.wait(2000);
    cy.url().should("contain", `${Cypress.config().baseUrl}`);
    cy.get(s.modules_container, t.timeout_L).should("be.visible");
  });

  it("Přidán přehled", () => {
    cy.goToAdministrationOf(s.templates_link);
    cy.contains(s.row, "CyPrehledTMP", t.timeout_M).should("not.exist");
    cy.addTemplate(
      "CyPrehledTMP",
      "overview",
      e.getEnv().moduleNameForEditTest
    );
    cy.contains(s.row, "CyPrehledTMP", t.timeout_L).should("be.visible");
  });
  it("Test NEviditelnosti prehledové", () => {
    cy.get(s.modules_container, t.timeout_L).within(() => {
      cy.contains(s.module_dropdown, e.moduleName, t.timeout_L).click();
      cy.get(s.dropdown_menu).within(() => {
        cy.contains(
          s.template_view_nav_link,
          "CyPrehledTMP",
          t.timeout_S
        ).should("not.exist");
      });
    });
  });
  it("Jednoduchá editace - prehledova", () => {
    cy.contains(s.row, "CyPrehledTMP", t.timeout_XL).within(() => {
      cy.get(s.edit_button).click();
    });
    cy.contains(
      s.modal_content,
      "Upravit detail řádku - CyPrehledTMP",
      t.timeout_M
    ).within(() => {
      cy.contains(s.input_container, "Název").within(() => {
        cy.get(s.flowio_text_input).clear().type("CyPrehledEditedTMP");
      });
      cy.contains(s.input_container, "Modul").within(() => {
        cy.get(s.flowio_select_input).select(e.moduleName);
      });
      cy.contains(s.input_container, "Viditelná").within(() => {
        cy.get(s.flowio_switch_input).click();
      });
      cy.get(s.submit_form_button).click();
    });
    cy.contains(s.modal_content, t.timeout_M).should("not.exist");
    cy.contains(s.row, "CyPrehledTMP", t.timeout_L).should("not.exist");
    cy.contains(s.row, "CyPrehledEditedTMP", t.timeout_M).should("be.visible");
  });
  it("Kontrola editace prehledové + NEzobrazení funkcí pro záznamy (editace v řádku atd)", () => {
    //na starem neni
    cy.get(s.modules_container, t.timeout_L).within(() => {
      cy.contains(
        s.module_dropdown,
        e.getEnv().moduleNameForEditTest,
        t.timeout_L
      ).click();
      cy.get(s.dropdown_menu).within(() => {
        cy.contains(
          s.template_view_nav_link,
          "CyPrehledEditedTMP",
          t.timeout_S
        ).should("not.be.visible");
      });
      //na novem je
      cy.contains(s.module_dropdown, e.moduleName, t.timeout_L).click();
      cy.get(s.dropdown_menu).within(() => {
        cy.contains(s.template_view_nav_link, "CyPrehledEditedTMP", t.timeout_S)
          .should("be.visible")
          .click();
      });
    });

    cy.get(s.refresh_button, t.timeout_XL).should("be.visible");
    cy.get(s.user_filter_button).should("be.visible");
    cy.get(s.add_record_button).should("not.exist");
    cy.get(s.add_row_record_button).should("not.exist");

    cy.get(s.table, t.timeout_XL).within(() => {
      cy.get(s.row, t.timeout_L).within(() => {
        cy.get(s.inline_edit_button).should("not.exist");
        cy.get(s.edit_button).should("not.exist");
        cy.get(s.delete_button).should("not.exist");
        cy.get(s.search_button).should("not.exist");
      });
    });
  });
  it("Přidán detail2", () => {
    cy.goToAdministrationOf(s.templates_link);
    cy.contains(s.row, "CyDetail2TMP", t.timeout_M).should("not.exist");
    cy.addTemplate("CyDetail2TMP", "detail", e.moduleName);
    cy.contains(s.row, "CyDetail2TMP", t.timeout_L).should("be.visible");
  });
  it("Přidána polozkova", () => {
    cy.contains(s.row, "CyPolozkovaTMP", t.timeout_M).should("not.exist");
    cy.addTemplate("CyPolozkovaTMP", "item", e.moduleName);
    cy.contains(s.row, "CyPolozkovaTMP", t.timeout_L).should("be.visible");
  });
  it("Přidán kalendář", () => {
    cy.contains(s.row, "CyKalendarTMP", t.timeout_M).should("not.exist");
    cy.addTemplate("CyKalendarTMP", "calendar", e.moduleName);
    cy.contains(s.row, "CyKalendarTMP", t.timeout_L).should("be.visible");
  });
  it("Přidána notifikacni", () => {
    cy.contains(s.row, "CyNotifTMP", t.timeout_M).should("not.exist");
    cy.addTemplate("CyNotifTMP", "notification", e.moduleName);
    cy.contains(s.row, "CyNotifTMP", t.timeout_XL).should("be.visible");
  });
  it("Kopie šablony - detail1", () => {
    cy.copyTemplate("CyEditace - D - P - D", "CyDetail1TMP");
    cy.get(s.templates_link).click();
    cy.contains(s.row, "CyDetail1TMP", t.timeout_L).should("be.visible");
  });
  it("Prehled - model - kontrola dat", () => {
    cy.contains(s.row, "CyPrehledEditedTMP").within(() => {
      cy.get(s.model_button).click();
    });
    cy.checkNavMenuTB("CyPrehledEditedTMP");
    cy.checkDataTB(
      "CyPrehledEditedTMP",
      "Přehled",
      e.getEnv().dataSourceName,
      e.tableName
    );
  });
  it("Přehled - nastaveni moznosti editace zaznamu", () => {
    cy.setActionsTB();
  });
  it("Přehled - navazani detailu + uložit", () => {
    cy.templateConnection("Detail", "CyDetail1TMP");
    cy.contains(
      "Při zapnuté editaci nebo vytváření a editaci musí mít přiřazený detail"
    ).should("not.exist");
    cy.get(s.save_button).click();
  });
  it("Položková - kontrola modelu", () => {
    cy.wait(3000);
    cy.goToAdministrationOf(s.templates_link);
    cy.wait(500);
    cy.contains(s.row, "CyPolozkovaTMP", t.timeout_L).within(() => {
      cy.get(s.model_button).click();
    });
    cy.checkNavMenuTB("CyPolozkovaTMP");
    cy.checkDataTB(
      "CyPolozkovaTMP",
      "Položková",
      e.getEnv().dataSourceName,
      e.tableName
    );
  });
  it("Položková - navazani detailu2 a vyber navazovaciho sloupce", () => {
    cy.templateConnection("Detail", "CyDetail2TMP");
    cy.get(s.template_builder_chose_join_column).within(() => {
      cy.get(s.flowio_select_input).select("ukol.cislo_subjektu");
    });
    cy.get(s.save_button).click();
  });
  it("Kontrola modelu - detail2", () => {
    cy.wait(3000);
    cy.goToAdministrationOf(s.templates_link);
    cy.contains(s.row, "CyDetail2TMP", t.timeout_L).within(() => {
      cy.wait(500);
      cy.get(s.model_button).click();
    });
    cy.checkNavMenuTB("CyDetail2TMP");
    cy.checkDataTB(
      "CyDetail2TMP",
      "Detail",
      e.getEnv().dataSourceName,
      e.tableName
    );
  });
  it("Navazani polozkove na detail1", () => {
    cy.goToAdministrationOf(s.templates_link);
    cy.wait(500);
    cy.contains(s.row, "CyDetail1TMP", t.timeout_L).within(() => {
      cy.get(s.model_button).click();
    });
    cy.templateConnection("Položková", "CyPolozkovaTMP");
    cy.get(s.template_builder_chose_join_column).within(() => {
      cy.get(s.flowio_select_input).select("ukol.cislo_subjektu");
    });
    cy.get(s.save_button).click();
  });
  it("Kontrola modelu kalendářové šablony + viditelnost.", () => {
    cy.wait(3000);
    cy.goToAdministrationOf(s.templates_link);
    cy.wait(500);
    cy.contains(s.row, "CyKalendarTMP", t.timeout_L).within(() => {
      cy.wait(500);
      cy.get(s.model_button).click();
    });
    cy.checkNavMenuTB("CyKalendarTMP");
    cy.checkDataTB(
      "CyKalendarTMP",
      "Kalendář",
      e.getEnv().dataSourceName,
      e.tableName
    );
    cy.contains(s.form_cell, "Viditelná").within(() => {
      cy.get(s.flowio_switch_input).click();
    });
    cy.get(s.save_button).click();
  });
  it("Kontrola modelu notifikační šablony + viditelnost.", () => {
    cy.wait(3000);
    cy.goToAdministrationOf(s.templates_link);
    cy.wait(500);
    cy.contains(s.row, "CyNotifTMP", t.timeout_L).within(() => {
      cy.get(s.model_button).click();
    });
    cy.checkNavMenuTB("CyNotifTMP");
    cy.checkDataTB(
      "CyNotifTMP",
      "Notifikační",
      e.getEnv().dataSourceName,
      e.tableName
    );
    cy.contains(s.form_cell, "Viditelná").within(() => {
      cy.get(s.flowio_switch_input).click();
    });
    cy.get(s.save_button).click();
  });
  it("Kontrola vytvořené šablony - kalendář.", () => {
    cy.wait(500);
    cy.get("#flowio-navbar-logo").click();
    cy.reload();
    cy.goToTemplate(e.moduleName, "CyKalendarTMP");
    cy.get(s.calendar, t.timeout_L).should("be.visible");
  });
  it("Kontrola vytvořené šablony - prehled+detail+polozkova.", () => {
    cy.wait(500);
    cy.goToTemplate(e.moduleName, "CyPrehledEditedTMP");
    cy.get(s.refresh_button, t.timeout_XL).should("be.visible");
    cy.get(s.user_filter_button).should("be.visible");
    cy.get(s.add_record_button).should("be.visible");
    cy.get(s.add_row_record_button).should("be.visible");

    cy.get(s.table, t.timeout_XL).within(() => {
      cy.get(s.row, t.timeout_L).within(() => {
        cy.get(s.inline_edit_button).should("be.visible");
        cy.get(s.edit_button).should("be.visible");
        cy.get(s.delete_button).should("be.visible");
        cy.get(s.search_button).should("be.visible");
      });
      cy.get(s.row)
        .first()
        .within(() => {
          cy.wait(500);
          cy.get(s.search_button).click();
        });
    });
    cy.get(s.template_detail_container, t.timeout_L).should("be.visible");
    cy.get(s.undo_button).should("be.visible");
    cy.get(s.edit_button).should("be.visible");
    cy.get(s.item_template_container).should("be.visible");

    cy.get(s.row)
      .first()
      .within(() => {
        cy.wait(500);
        cy.get(s.search_button).click();
      });
    cy.contains(s.modal_content, "Detail záznamu").should("be.visible");
    cy.get(".close").click();
  });
  it("Kontrola vytvořené šablony - notifikační.", () => {
    cy.wait(500);
    cy.get(s.notifications_icon).click();
    cy.get(s.notifications_dropdown_menu).within(() => {
      cy.get(s.settings_image).click();
    });
    cy.get(s.modal_content, t.timeout_M).within(() => {
      cy.contains(s.selected_templates, "CyNotifTMP");
      cy.get(".close").click();
    });
  });
  it("Smazání šablon.", () => {
    cy.goToAdministrationOf(s.templates_link);
    cy.deleteTemplate("CyPrehledEditedTMP");
    cy.deleteTemplate("CyDetail1TMP");
    cy.deleteTemplate("CyNotifTMP");
    cy.deleteTemplate("CyKalendarTMP");
    cy.deleteTemplate("CyPolozkovaTMP");
    cy.deleteTemplate("CyDetail2TMP");
  });
});
