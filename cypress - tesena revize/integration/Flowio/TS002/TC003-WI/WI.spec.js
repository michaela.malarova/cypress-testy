/// <reference types="cypress" />
import * as s from "../../../../support/selectors/All-selectors";
import * as e from "../../../../support/envConfig/variables/envVar";
import * as t from "../../../../support/envConfig/variables/timeouts";

Cypress.Cookies.defaults({
  preserve: [
    e.adminGuided_cookieName,
    e.alreadyGuided_cookieName,
    e.getEnv().loginCookieName,
    e.getEnv().XSRF_TOKEN_DEV_CookieName,
    e.getEnv().AspNetCore_Antiforgery_CookieName,
  ],
});

context("WI", () => {
  before(() => {
    cy.clearCookies({ domain: null });
    cy.setCookie(e.adminGuided_cookieName, e.adminGuided_cookieValue); //-to co se me pta na tutorial adminas
    cy.setCookie(e.alreadyGuided_cookieName, e.alreadyGuided_cookieValue);
  });

  it("Přechod do FLOWIA", () => {
    //nic uz s tim proboha nedelej
    cy.visit("/");
    cy.wait(2000);
    cy.url().should("contain", `${Cypress.config().baseUrl}`);
    cy.get(s.modules_container, t.timeout_L).should("be.visible");
  });
  it("Prechod na sablonu CyWI.", () => {
    cy.wait(500);
    cy.goToTemplate(e.moduleName, "CyWI");
  });
  it("Kontrola filtrovani zaznamů - loginId, název, rozmezí dat, číslo.", () => {
    cy.get(s.row, t.timeout_L).each(($el) => {
      cy.get($el).within(() => {
        //podminka na login id
        cy.get(s.recordAttr_login_id)
          .invoke("text")
          .then((loginId) => {
            cy.get(s.recordAttr_string_nazevSub)
              .invoke("text")
              .then((nazev) => {
                if (
                  nazev != "CyDynDatum" &&
                  nazev != "CyDynDatum 2" &&
                  loginId != "flowio_docker"
                ) {
                  cy.log(
                    "filtrovani zaznamu: nazev != CyDynDatum or loginId != flowio_docker"
                  );
                  cy.get(false);
                }
              });
          });
        cy.get(`${s.recordAttr_dateTime} > span`)
          .invoke("text")
          .then((text) => {
            const [date, time] = text.split(" ");
            const [day, month, year] = date.split(".");
            const [hours, minutes, seconds] = time.split(":");
            if (month != "01" || year != "2021") {
              //filtr je jen na dny z tohoto mesice
              var recordYear = Number(year);
              var recordMonth = Number(month);
              var recordDay = Number(day);
              var today = new Date();
              //today.setHours(today.getHours() - e.getEnv().timeShift); //protoze docker
              var currentYear = today.getFullYear();
              var currentMonth = today.getMonth() + 1;
              var currentDay = today.getDate();
              if (
                currentYear != recordYear ||
                recordMonth != currentMonth ||
                currentDay != recordDay
              ) {
                //pokud datum neni dnesni tak false (filtr na dyn datum)
                cy.log("filtrovani zaznamu: datum není dnesni");
                cy.get(false);
              }
            }
          });
        cy.get(s.recordAttr_decimal_pocetDniRealita)
          .invoke("text")
          .then((cislo) => {
            if (cislo != "") {
              var num = Number(cislo);
              if (!(num < 10)) {
                cy.log("filtrovani zaznamu: decimal !< 10");
                cy.get(false);
              }
            } else {
              cy.log("filtrovani zaznamu: decimal = null");
              cy.get(false);
            }
          });
      });
    });
  });
  it("CleanUp", () => {
    var deleteRowId = [];
    cy.get(s.row)
      .each(($el) => {
        cy.get($el).within(() => {
          cy.get(s.recordAttr_string_nazevSub)
            .invoke("text")
            .then((nazev) => {
              if (nazev == "CyDynDatum" || nazev == "CyDynDatum 2") {
                cy.get('[data-cy="subjekty5.reference_subjektu-input-column"]')
                  .invoke("text")
                  .then((value) => {
                    deleteRowId.push(value);
                  });
              }
            });
        });
      })
      .then(() => {
        if (deleteRowId.length > 0) {
          cy.get(deleteRowId).then((node) => {
            for (var i = 0; i < deleteRowId.length; i++) {
              cy.contains(s.row, node[i], t.timeout_M).within(() => {
                cy.get(s.delete_button).click();
              });
              cy.get(s.modal_content).within(() => {
                cy.get(s.accept_button).click();
              });
            }
          });
        }
      });
  });
  it("Kontrola filtrovani zaznamů - dynamické datum.", () => {
    cy.wait(500);
    //pridat zaznam jen s vyhovujicim datem
    cy.get(s.add_row_record_button, t.timeout_L).click();
    //pridam zaznam co vyhovuje dynamickemu datu
    cy.get(s.new_row, t.timeout_L).within(() => {
      cy.get(s.recordAttr_string_nazevSub, t.timeout_XL).type("CyDynDatum");
      cy.get(s.recordAttr_int_naplanovano).type(0);
      cy.get(s.recordAttr_decimal_pocetDniRealita).type(2);
      cy.get(s.recordAttr_string_stav_Select).within(() => {
        cy.get(s.flowio_select_input).select("dva");
      });
      cy.get(s.recordAttr_string_vlastnik_Select).within(() => {
        cy.get(s.flowio_select_input).select(e.value_string_vlastnik);
      });
    });

    var today = new Date();
    var currentYear = today.getFullYear();
    var currentMonth = today.getMonth() + 1;
    var currentDay = today.getDate();

    var dateInput;
    if (currentDay > 9 && currentMonth > 9) {
      dateInput = `${currentDay}${currentMonth}${currentYear}0200`;
      cy.get(s.new_row, t.timeout_L).within(() => {
        cy.get(s.recordAttr_dataTime_datumPorizeni, t.timeout_L).within(() => {
          cy.get("input", t.timeout_M).clear().type(dateInput);
        });
        cy.get(s.accept_button).click();
      });
      cy.contains(s.row, "CyDynDatum", t.timeout_XL).within(() => {
        cy.get(s.recordAttr_dataTime_datumPorizeni, t.timeout_XL).should(
          "have.text",
          `${currentDay}.${currentMonth}.${currentYear} ${
            e.getEnv().timeShift
          }:00:00`
        );
      });
    } else if (currentDay > 9 && currentMonth < 10) {
      dateInput = `${currentDay}0${currentMonth}${currentYear}0200`;
      cy.get(s.new_row, t.timeout_L).within(() => {
        cy.get(s.recordAttr_dataTime_datumPorizeni, t.timeout_L).within(() => {
          cy.get("input", t.timeout_M).clear().type(dateInput);
        });
        cy.get(s.accept_button).click();
      });
      cy.contains(s.row, "CyDynDatum", t.timeout_XL).within(() => {
        cy.get(s.recordAttr_dataTime_datumPorizeni, t.timeout_XL).should(
          "have.text",
          `${currentDay}.0${currentMonth}.${currentYear} ${
            e.getEnv().timeShift
          }:00:00`
        );
      });
    } else if (currentDay < 10 && currentMonth > 9) {
      dateInput = `0${currentDay}${currentMonth}${currentYear}0200`;
      cy.get(s.new_row, t.timeout_L).within(() => {
        cy.get(s.recordAttr_dataTime_datumPorizeni, t.timeout_L).within(() => {
          cy.get("input", t.timeout_M).clear().type(dateInput);
        });
        cy.get(s.accept_button).click();
      });
      cy.contains(s.row, "CyDynDatum", t.timeout_XL).within(() => {
        cy.get(s.recordAttr_dataTime_datumPorizeni, t.timeout_XL).should(
          "have.text",
          `0${currentDay}.${currentMonth}.${currentYear} ${
            e.getEnv().timeShift
          }:00:00`
        );
      });
    } else {
      dateInput = `0${currentDay}0${currentMonth}${currentYear}0200`;
      cy.get(s.new_row, t.timeout_L).within(() => {
        cy.get(s.recordAttr_dataTime_datumPorizeni, t.timeout_L).within(() => {
          cy.get("input", t.timeout_M).clear().type(dateInput);
        });
        cy.get(s.accept_button).click();
      });
      cy.contains(s.row, "CyDynDatum", t.timeout_XL).within(() => {
        cy.get(s.recordAttr_dataTime_datumPorizeni, t.timeout_XL).should(
          "have.text",
          `0${currentDay}.0${currentMonth}.${currentYear} ${
            e.getEnv().timeShift
          }:00:00`
        );
      });
    }
  });
  /*
  it("Kontrola predvyplnovani v radku.", () => {
    //zkontroluju jestli je to vyplnenen tak jak ma (podminky NEjsou splnene)
    cy.search("CyDynDatum"); //nazev nebudu uplne kontrolovat, zbytecne
    cy.wait(500);
    cy.contains(s.row, "CyDynDatum", t.timeout_M).within(() => {
      cy.get(s.recordAttr_string_stav_Select)
        .invoke("text")
        .should("eq", "dva");
      cy.get(s.recordAttr_decimal_pocetDniRealita)
        .invoke("text")
        .should("eq", "2");
      //pole co se bude predvyplnovat (10)
      cy.get(s.recordAttr_int_naplanovano).invoke("text").should("eq", "0");
      cy.get(s.inline_edit_button).click();
    });
    //jsou 3 podminky - vzdy jednu NEsplnim - predvyplni se az budou splnene VSECHNY
    //není splnena 2. podminka (stav = 2):
    cy.get(s.row, t.timeout_L).within(() => {
      cy.get(s.recordAttr_string_nazevSub).within(() => {
        cy.get(s.flowio_text_input).clear().type("CyDynDatum 2");
      });
      cy.get(s.recordAttr_decimal_pocetDniRealita).within(() => {
        cy.get(s.flowio_number_input).clear().type(1);
      });
    });
    cy.get(s.refresh_button).click();
    /*cy.get(s.row, t.timeout_L).within(() => {   NEJDE - pri editaci se nejde koukat na hodnotu v poli, stejne tohle bylo jen pro kontrolu
      cy.get(s.recordAttr_int_naplanovano).invoke("text").should("eq", "2");
    });/
    //není splnena 1. podminka (nazev):
    cy.get(s.row, t.timeout_L).within(() => {
      cy.get(s.recordAttr_string_nazevSub).within(() => {
        cy.get(s.flowio_text_input).clear().type("CyDynDatum");
      });
      cy.get(s.recordAttr_string_stav_Select).within(() => {
        cy.get("select").select("tri");
      });
    });
    cy.get(s.refresh_button).click();
    cy.get(s.row, t.timeout_L).within(() => {
      cy.get(s.recordAttr_int_naplanovano).within(() => {
        cy.get("input").invoke("attr", "value").should("eq", "0");
      });
    });
    //neni splnena 3. podminka
    cy.get(s.row, t.timeout_L).within(() => {
      cy.get(s.recordAttr_decimal_pocetDniRealita).within(() => {
        cy.get(s.flowio_number_input).clear().type(0);
      });
      cy.get(s.recordAttr_string_nazevSub).within(() => {
        cy.get(s.flowio_text_input).clear().type("CyDynDatum 2");
      });
    });
    cy.get(s.refresh_button).click();
    cy.get(s.row, t.timeout_L).within(() => {
      cy.get(s.recordAttr_int_naplanovano).within(() => {
        cy.get("input").invoke("attr", "value").should("eq", "0");
      });
    });
    //vse je splneno (+ kontrola or):
    cy.get(s.row, t.timeout_L).within(() => {
      cy.get(s.recordAttr_decimal_pocetDniRealita).within(() => {
        cy.get(s.flowio_number_input).clear().type(1);
      });
    });
    cy.get(s.refresh_button).click();
    cy.get(s.row, t.timeout_L).within(() => {
      cy.get(s.recordAttr_int_naplanovano).within(() => {
        cy.get("input").invoke("attr", "value").should("eq", "10");
      });
    });

    cy.get(s.row, t.timeout_L).within(() => {
      cy.get(s.recordAttr_decimal_pocetDniRealita).within(() => {
        cy.get(s.flowio_number_input).clear().type(-1); //or
      });
      cy.get(s.recordAttr_int_naplanovano).within(() => {
        cy.get(s.flowio_number_input).clear();
      });
    });
    cy.get(s.refresh_button).click();
    cy.get(s.row, t.timeout_L).within(() => {
      cy.get(s.recordAttr_int_naplanovano).within(() => {
        cy.get("input").invoke("attr", "value").should("eq", "10");
      });
    });
    //po ulozeni zustane predvyplneno
    cy.wait(500);
    cy.get(s.row, t.timeout_L).within(() => {
      cy.wait(500);
      cy.get(s.accept_button).click();
    });
    cy.search("CyDynDatum 2"); //nazev nebudu uplne kontrolovat, zbytecne
    cy.contains(s.row, "CyDynDatum 2", t.timeout_M).within(() => {
      cy.get(s.recordAttr_int_naplanovano).invoke("text").should("eq", "10");
    });
  });
  it("Test podminek zobrazeni/editace/predvyplneni na detailu.", () => {
    cy.search("CyDynDatum");
    cy.get(s.row, t.timeout_L).within(() => {
      cy.get(s.search_button).click();
    });
    cy.get(s.template_detail_container, t.timeout_XL)
      .should("be.visible")
      .within(() => {
        //string je viditelny kdyz int=5 or 6 (nyni je int = 10)
        cy.contains("tr", "string", t.timeout_M).should("not.exist");
        //pro kontrolu predvyplneni - stav pred - pak se predvyplni na "dva"
        cy.contains("tr", "stat.vyber", t.timeout_M)
          .contains("tri")
          .should("be.visible");
      });
    cy.get(s.table_data_item_header).within(() => {
      cy.get(s.edit_button).click();
    });
    //decimal je needitovatelné - musi byt int =6 and string = CyEditovatelnost
    cy.contains("tr", "decimal", t.timeout_M).within(() => {
      //pokud je needitovatelny, nema pole v sobe input
      cy.get("input").should("not.exist");
    });
    //zmenim int na 5, string bude viditelny
    cy.contains("tr", "int", t.timeout_M).within(() => {
      cy.get(s.flowio_number_input).clear().type(5);
    });
    cy.get(s.submit_button).click();
    cy.contains("tr", "string", t.timeout_L).should("be.visible");
    cy.contains("tr", "stat.vyber", t.timeout_M)
      .contains("tri")
      .should("be.visible");
    cy.get(s.table_data_item_header).within(() => {
      cy.get(s.edit_button).click();
    });
    cy.contains("tr", "decimal", t.timeout_M).within(() => {
      cy.get("input").should("not.exist");
    });
    cy.contains("tr", "int", t.timeout_M).within(() => {
      cy.get(s.flowio_number_input).clear().type(6);
    });
    cy.contains("tr", "string", t.timeout_M).within(() => {
      cy.get(s.flowio_text_input).clear().type("CyDynDatum predvyplnovani");
    });
    cy.get(s.refresh_button).click();
    cy.wait(1000);
    cy.get(s.submit_button).click();
    cy.contains("tr", "string", t.timeout_L).should("be.visible");
    cy.contains("tr", "stat.vyber", t.timeout_M)
      .contains("ctyri")
      .should("be.visible");
    cy.get(s.table_data_item_header).within(() => {
      cy.get(s.edit_button).click();
    });
    cy.contains("tr", "string", t.timeout_M).within(() => {
      cy.get(s.flowio_text_input).clear().type("CyDynDatum editovatelnost");
    });
    cy.get(s.submit_button).click();
    cy.contains("tr", "string", t.timeout_L).should("be.visible");
    cy.contains("tr", "stat.vyber", t.timeout_M)
      .contains("ctyri")
      .should("be.visible");
    cy.get(s.table_data_item_header).within(() => {
      cy.get(s.edit_button).click();
    });
    cy.contains("tr", "decimal", t.timeout_M).within(() => {
      cy.get("input").type(0);
    });
    cy.get(s.refresh_button).click();
    cy.contains("tr", "int", t.timeout_M).within(() => {
      cy.get(s.flowio_number_input).clear().type(10);
    });
    cy.get(s.submit_button).click();
    cy.contains("tr", "string", t.timeout_L).should("not.exist");
    cy.contains("tr", "stat.vyber", t.timeout_M)
      .contains("tri")
      .should("be.visible");
    cy.get(s.table_data_item_header).within(() => {
      cy.get(s.edit_button).click();
    });
    cy.contains("tr", "decimal", t.timeout_M).within(() => {
      cy.get("input").should("not.exist");
    });
  });
  it("Odfiltrovani testovach zaznamu.", () => {
    cy.contains("tr", "dataTime", t.timeout_M).within(() => {
      cy.get("input").clear().type("150120210000");
    });
    cy.get(s.submit_button).click();
    cy.get(s.edit_button, t.timeout_XL).should("be.visible");
    cy.wait(1000);
    cy.get(s.loader_image, t.timeout_XL).should("not.exist");
    cy.get(s.undo_button).click();
    cy.search("CyDynDatum editovatelnost");
    cy.wait(500);
    cy.get(s.row, t.timeout_XL).within(() => {
      cy.get(s.inline_edit_button, t.timeout_M).click();
    });
    cy.get(s.row).within(() => {
      cy.get(s.recordAttr_dateTime).within(() => {
        cy.get("input").clear().type("011020210000");
      });
      cy.get(s.accept_button).click();
    });
    cy.wait(1000);
    cy.contains(s.row, "CyDynDatum", t.timeout_M).should("not.exist");
  });
  */
});
