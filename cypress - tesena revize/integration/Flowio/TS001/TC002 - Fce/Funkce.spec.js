/// <reference types="cypress" />
import * as s from "../../../../support/selectors/All-selectors";
import * as e from "../../../../support/envConfig/variables/envVar";
import * as t from "../../../../support/envConfig/variables/timeouts";

Cypress.Cookies.defaults({
  preserve: [
    e.getEnv().loginCookieName,
    e.adminGuided_cookieName,
    e.alreadyGuided_cookieName,
    e.getEnv().XSRF_TOKEN_DEV_CookieName,
    e.getEnv().AspNetCore_Antiforgery_CookieName,
  ],
});

context("Funkce", () => {
  before(() => {
    cy.clearCookies({ domain: null }); //zkusit jestli je to potrebne ??????
    cy.setCookie(e.adminGuided_cookieName, e.adminGuided_cookieValue);
    cy.setCookie(e.alreadyGuided_cookieName, e.alreadyGuided_cookieValue);
  });
  it("Přechod na stránku pro přihlášení.", () => {
    cy.visit("/");
    cy.wait(2000);
    cy.url().should("contain", `/login`);
    cy.get(s.form, t.timeout_L).should("be.visible");
  });
  it("Přihlášení - FLOWIO.", () => {
    cy.login(e.getEnv().loginName, e.getEnv().loginPassword);
    cy.url().should("contain", `${Cypress.config().baseUrl}`);
    cy.get(s.modules_container, t.timeout_L).should("be.visible");
  });
  const vhodneRadkyP1 = [];
  const vhodneRadkyP2 = [];
  const neVhodneRadky = [];
  it("Přechod na šablonu.", () => {
    cy.visit("/");
    cy.wait(1000);
    cy.goToTemplate(e.moduleName, "CyFCE");
  });
  it("Test podminek zobrazeni funkce na prehledu.", () => {
    //roztridim sio zaznamy
    cy.get(s.row)
      .each(($el) => {
        // $el is a wrapped jQuery element
        let row = {
          id: null,
          stav: null,
          int: null,
        };
        cy.wrap($el).within(() => {
          cy.get('[data-cy="ukol.cislo_subjektu-input-column"]')
            .invoke("text")
            .then((value) => {
              row.id = value;
              cy.log(`id ${row.id}`);
            });
          cy.get(s.recordAttr_string_stav_Select)
            .invoke("text")
            .then((value) => {
              row.stav = value;
              cy.log(`stav ${row.stav}`);
            });
          cy.get(s.recordAttr_int_naplanovano)
            .invoke("text")
            .then((value) => {
              row.int = value;
              if (row.int != "") {
                cy.log(`nevhorny radek ${row.id}`);
                neVhodneRadky.push(row);
              } else if (row.stav == 1) {
                cy.log(`P1 radek ${row.id}`);
                vhodneRadkyP1.push(row);
              } else {
                cy.log(`P2 radek ${row.id}`);
                vhodneRadkyP2.push(row);
              }
            });
        });
      })
      //nevhodne radky nemaji moznost pusteni fce v řádku
      .then(() => {
        cy.get(neVhodneRadky).then((node) => {
          for (var i = 0; i < neVhodneRadky.length; i++) {
            cy.wait(1000);
            cy.contains(s.row, node[i].id, t.timeout_M).within(() => {
              cy.wait(1000);
              cy.get(s.more_button).click();
            });
            cy.get(
              '[style="position: fixed; z-index: 1300; inset: 0px;"]',
              t.timeout_L
            )
              .contains("Tento seznam neobsahuje žádné položky")
              .should("be.visible");
            cy.get('[style="position: fixed; z-index: 1300; inset: 0px;"]')
              .contains("Funkce bez šablony (Prepnout stav)")
              .should("not.exist");
            cy.get('[id="flowio-container"]').click({ force: true });
            cy.get(s.refresh_button).click({ force: true });
          }
        });
      })
      //vhodne radky maji moznost pustit fci v radku
      .then(() => {
        cy.get(vhodneRadkyP1).then((node) => {
          for (var i = 0; i < vhodneRadkyP1.length; i++) {
            cy.wait(1000);
            cy.contains(s.row, node[i].id, t.timeout_M).within(() => {
              cy.wait(1000);
              cy.get(s.more_button).click();
            });
            cy.wait(1000);
            cy.get('[style="position: fixed; z-index: 1300; inset: 0px;"]')
              .contains("Funkce bez šablony (Prepnout stav)")
              .should("be.visible");
            cy.get('[style="position: fixed; z-index: 1300; inset: 0px;"]')
              .contains("Tento seznam neobsahuje žádné položky")
              .should("not.exist");
            cy.wait(1000);
            cy.get('[id="flowio-container"]').click({ force: true });
            cy.get(s.refresh_button).click({ force: true });
          }
        });
      })
      //vhodne radky maji moznost pustit fci v radku
      .then(() => {
        cy.get(vhodneRadkyP2).then((node) => {
          for (var i = 0; i < vhodneRadkyP2.length; i++) {
            cy.wait(1000);
            cy.contains(s.row, node[i].id, t.timeout_M).within(() => {
              cy.wait(1000);
              cy.get(s.more_button).click();
            });
            cy.wait(1000);
            cy.get('[style="position: fixed; z-index: 1300; inset: 0px;"]')
              .contains("Funkce bez šablony (Prepnout stav)")
              .should("be.visible");
            cy.get('[style="position: fixed; z-index: 1300; inset: 0px;"]')
              .contains("Tento seznam neobsahuje žádné položky")
              .should("not.exist");
            cy.wait(1000);
            cy.get('[id="flowio-container"]').click({ force: true });
            cy.get(s.refresh_button).click({ force: true });
          }
        });
      })
      //test pusteni funkce v řádku
      .then(() => {
        cy.get(vhodneRadkyP1).then((node) => {
          cy.contains(s.row, node[0].id, t.timeout_M).within(() => {
            cy.wait(500);
            cy.get(s.recordAttr_string_stav_Select).should("have.text", "1");
          });
          cy.wait(1000);
          cy.contains(s.row, node[0].id, t.timeout_M).within(() => {
            cy.wait(1000);
            cy.get(s.more_button).click();
          });
          cy.get(
            '[style="position: fixed; z-index: 1300; inset: 0px;"]',
            t.timeout_M
          )
            .contains("Funkce bez šablony (Prepnout stav)")
            .click();
          cy.wait(1000);
          cy.contains(s.row, node[0].id, t.timeout_M).within(() => {
            cy.wait(500);
            cy.get(s.recordAttr_string_stav_Select).should("have.text", "2");
          });
          //tzn vhodneRadkyP1[0].stav = 2
        });
      });
  });
  it("test podminek zobrazeni polozkovych  + test pusteni fce na detailu.", () => {
    cy.contains(s.row, vhodneRadkyP2[0].id).within(() => {
      cy.wait(500);
      cy.get(s.search_button).click();
    });
    cy.contains("CyFCE - D - P2", t.timeout_L).should("be.visible");
    cy.contains("CyFCE - D - P1", t.timeout_M).should("not.exist");

    cy.get(s.template_detail_container, t.timeout_M).within(() => {
      cy.contains("tr", "stav").contains("2");
      cy.contains("Funkce bez šablony (Prepnout stav)").click();
      cy.wait(6000);
      cy.contains("tr", "stav", t.timeout_M).contains("1", t.timeout_M);
      //tzn vhodneRadkyP2[0].stav = 1
    });
    cy.contains("CyFCE - D - P2", t.timeout_M).should("not.exist");
    cy.contains("CyFCE - D - P1", t.timeout_M).should("be.visible");
    cy.goToTemplate(e.moduleName, "CyFCE");
  });
  /*
  it("Pusteni hromadne funkce pro nevhodné záznamy", () => {
    cy.get(neVhodneRadky).then((node) => {
      cy.contains(s.row, node[0].id).within(() => {
        cy.get('[type="checkbox"]').click();
      });
    });
    cy.get(vhodneRadkyP1).then((node) => {
      cy.contains(s.row, node[0].id).within(() => {
        cy.get(s.recordAttr_string_stav_Select).contains("2");
        cy.get('[type="checkbox"]').click();
      });
    });
    cy.get(vhodneRadkyP2).then((node) => {
      cy.contains(s.row, node[0].id).within(() => {
        cy.get(s.recordAttr_string_stav_Select).contains("1");
        cy.get('[type="checkbox"]').click();
      });
    });
    cy.contains("Vyberte funkci").click();
    cy.contains("li", "Funkce bez šablony (Prepnout stav)", t.timeout_M).should(
      "not.exist"
    );
    cy.contains(
      "li",
      "Nad vybranými záznamy nelze spustit žádnou funkci",
      t.timeout_M
    ).should("be.visible");
    // nejde mi se vykliknout, hard restart =>
    cy.visit("/");
    cy.wait(1000);
    cy.goToTemplate(e.moduleName, "CyFCE");
    cy.get(vhodneRadkyP1).then((node) => {
      cy.contains(s.row, node[0].id).within(() => {
        cy.get(s.recordAttr_string_stav_Select).contains("2");
        cy.get('[type="checkbox"]').click();
      });
    });
    cy.get(vhodneRadkyP2).then((node) => {
      cy.contains(s.row, node[0].id).within(() => {
        cy.get(s.recordAttr_string_stav_Select).contains("1");
        cy.get('[type="checkbox"]').click();
      });
    });
    cy.contains("Vyberte funkci").click();
    cy.contains(
      "li",
      "Nad vybranými záznamy nelze spustit žádnou funkci",
      t.timeout_M
    ).should("not.exist");
    cy.contains(
      "li",
      "Funkce bez šablony (Prepnout stav)",
      t.timeout_M
    ).click();
    //pak sem pridat atribut ted mi lejde local5555
    cy.contains('[data-cy~="button"]', "play_arrow").click();
    cy.wait(1000);
    cy.get(vhodneRadkyP1).then((node) => {
      cy.contains(s.row, node[0].id).within(() => {
        cy.get(s.recordAttr_string_stav_Select).contains("1");
      });
    });
    cy.get(vhodneRadkyP2).then((node) => {
      cy.contains(s.row, node[0].id).within(() => {
        cy.get(s.recordAttr_string_stav_Select).contains("2");
      });
    });
  });
  */
});
