/// <reference types="cypress" />
import * as s from "../../../../support/selectors/TS001/TC005-Notifikace/Notifikace-selectors";
import * as e from "../../../../support/envConfig/variables/envVar"; // base url podle ni to prepinat
import * as t from "../../../../support/envConfig/variables/timeouts";
import * as r from "../../../../support/envConfig/req/záznamDocker";

Cypress.Cookies.defaults({
  preserve: [
    e.getEnv().loginCookieName,
    e.adminGuided_cookieName,
    e.alreadyGuided_cookieName,
  ],
});

context("Notifikace", () => {
  before(() => {
    cy.clearCookies({ domain: null });
    cy.setCookie(e.adminGuided_cookieName, e.adminGuided_cookieValue); //-to co se me pta na tutorial adminas
    cy.setCookie(e.alreadyGuided_cookieName, e.alreadyGuided_cookieValue);
  });

  it("Přihlášení - FLOWIO.", () => {
    cy.visit("/");
    cy.url().should("contain", `${Cypress.config().baseUrl}`);
    cy.get(s.modules_container, t.timeout_L).should("be.visible");
  });
  it("Seznam notifikací - zobrazení.", () => {
    cy.waitForLoading(s.notifications_link);
    cy.get(s.notifications_link, t.timeout_L).click();
    cy.get(s.notifications_dropdown_menu, t.timeout_XS)
      .should("be.visible")
      .within(() => {
        cy.contains(s.notification_row, e.getEnv().testNotifTitle, t.timeout_L)
          .should("be.visible")
          .within(() => {
            cy.get(s.template_name, { timeout: 10000 })
              .should("be.visible")
              .should("have.text", e.getEnv().testNotifFromTemplateName);
            cy.get(s.notification_title, { timeout: 10000 })
              .should("be.visible")
              .should("have.text", e.getEnv().testNotifTitle);
            cy.get(s.delete_button, { timeout: 10000 }).should("be.visible");
          });
      })
      .within(() => {
        cy.get(s.settings_image, { timeout: 10000 }).should("be.visible");
      });
  });
  /* TO NA DOCKERU NEJDE BOHUZEL
  it("Proklik na šablonu ze seznamu notifikací", () => {
    cy.get(s.notifications_link, { timeout: 60000 }).click();
    cy.get(s.notification_link, { timeout: 10000 })
      .contains(e.getEnv().testNotifTitle)
      .click();
    cy.url({ timeout: 60000 }).should("contain", `overview`);
    cy.get(s.template_detail_container, { timeout: 60000 }).should(
      "be.visible"
    );
    cy.get(s.undo_button, { timeout: 5000 }).click();
    cy.url({ timeout: 60000 }).should("not.contain", `overview`);
  });
*/
  it("Nastavení notifikací - odebrat šablonu.", () => {
    cy.reload(); //jinak zase nejde klikat
    cy.waitForLoading(s.notifications_link);
    cy.get(s.notifications_link, t.timeout_S).click();
    cy.get(s.notifications_dropdown_menu, t.timeout_S)
      .should("be.visible")
      .within(() => {
        cy.get(s.settings_image, t.timeout_S).click();
      });
    cy.get(s.modal_content, t.timeout_L)
      .should("be.visible")
      .within(() => {
        cy.get(s.selected_templates, t.timeout_L).within(() => {
          cy.get(s.search_field).should("be.visible");
          cy.get(s.option_list_no_search, { timeout: 5000 }).select(
            e.getEnv().testNotifTemplateName
          );
        });
        cy.get(s.move_left_icon).click();
        cy.get(s.selected_templates, { timeout: 10000 })
          .contains(e.testNotifTemplateId, t.timeout_XS)
          .should("not.exist");
        cy.get(s.save_button).click();
      });
  });
  it("Odebrané notifikace se nezobrazují.", () => {
    cy.reload(); //jinak nejde kliknout
    cy.waitForLoading(s.notifications_link);
    cy.get(s.notifications_link, t.timeout_L).should("be.visible").click();
    cy.get(s.notifications_dropdown_menu, t.timeout_XS)
      .should("be.visible")
      .within(() => {
        cy.wait(1000);
        cy.contains(
          s.notification_row,
          e.getEnv().testNotifTitle,
          t.timeout_L
        ).should("not.exist");
      });
  });
  it("Nastavení notifikací - přidat šablonu.", () => {
    cy.reload(); //jinak nejde kliknout
    cy.waitForLoading(s.notifications_link);
    cy.get(s.notifications_link, t.timeout_S).click();
    cy.get(s.notifications_dropdown_menu, t.timeout_XXS)
      .should("be.visible")
      .within(() => {
        cy.get(s.settings_image, t.timeout_XXS).click();
      });
    cy.get(s.modal_content, t.timeout_S)
      .should("be.visible")
      .within(() => {
        cy.get(s.unselected_templates, t.timeout_L).within(() => {
          cy.get(s.search_field).should("be.visible");
          cy.get(s.option_list_no_search, { timeout: 5000 }).select(
            e.getEnv().testNotifTemplateName
          );
        });
        cy.get(s.move_right_icon).click();
        cy.get(s.unselected_templates, { timeout: 10000 })
          .contains(e.testNotifTemplateId, t.timeout_XS)
          .should("not.exist");
        cy.get(s.save_button).click();
      });
  });
  /*
  it("Přidání notifikace.", () => {
    cy.wait(4000);
    cy.get(s.notifications_link, t.timeout_L).click();
    cy.get(s.notifications_dropdown_menu, t.timeout_XS)
      .should("be.visible")
      .within(() => {
        cy.contains(
          s.notification_row,
          e.testNotifNewRecordName,
          t.timeout_L
        ).should("not.exist");
      });
    cy.request("POST", e.getEnv().reqURLnewRecord, r.newRecord);    //POMOC???????
    cy.reload();
    cy.waitForLoading(s.notifications_link);
    cy.get(s.notifications_link, t.timeout_L).click();
    cy.get(s.notifications_dropdown_menu, t.timeout_XS)
      .should("be.visible")
      .within(() => {
        cy.contains(
          s.notification_row,
          e.testNotifNewRecordName,
          t.timeout_L
        ).should("be.visible");
      });
  });

  it("Smazání notifikace.", () => {
    cy.waitForLoading(s.notifications_link);
    cy.get(s.notifications_link, t.timeout_L).click();
    cy.get(s.notifications_dropdown_menu, t.timeout_XS)
      .should("be.visible")
      .within(() => {
        cy.contains(
          s.notification_row,
          e.testNotifNewRecordName,
          t.timeout_L
        ).within(() => {
          cy.get(s.delete_button).click();
        });
      });

    cy.get(s.notifications_dropdown_menu, t.timeout_XS)
      .should("be.visible")
      .within(() => {
        cy.contains(
          s.notification_row,
          e.getEnv().testNotifTitle,
          t.timeout_L
        ).should("not.exist");
      });
  });
*/
  /*it("Uklid po testu.", () => {
    cy.get(s.notifications_link, t.timeout_S).click();
    cy.get(s.notifications_dropdown_menu, t.timeout_XXS)
      .should("be.visible")
      .within(() => {
        cy.get(s.settings_image, t.timeout_XXS).click();
      });
    cy.get(s.modal_content, t.timeout_S)
      .should("be.visible")
      .within(() => {
        cy.get(s.unselected_templates, t.timeout_L).within(() => {
          cy.get(s.option_list_no_search, { timeout: 5000 }).select(
            e.testNotifTemplateId
          );
        });
        cy.get(s.move_right_icon).click();
        cy.get(s.save_button).click();
      });
    cy.get(s.notifications_dropdown_menu, t.timeout_XS).within(() => {
      cy.get(s.notification_row, t.timeout_L)
        .should("be.visible")
        .within(() => {
          cy.get(s.template_name, { timeout: 10000 }).should(
            "have.text",
            e.TemplateNamesFromNotif
          );
          cy.get(s.notification_title, { timeout: 10000 }).should(
            "have.text",
            e.NotifTitles
          );
        });
    });
  });*/
});
