/// <reference types="cypress" />
import * as s from "../../../../support/selectors/TS001/TC001-Login/Login-selectors";
import * as e from "../../../../support/envConfig/variables/envVar";
import * as t from "../../../../support/envConfig/variables/timeouts";

Cypress.Cookies.defaults({
  preserve: [
    e.adminGuided_cookieName, //
    e.alreadyGuided_cookieName, //aby se aplikace nedotazovala na pruvodce aplikací
    e.getEnv().XSRF_TOKEN_DEV_CookieName, //nvm bez ni to blbne
    e.getEnv().AspNetCore_Antiforgery_CookieName, //nvm bez ni to blbne
  ],
});

context("Login", () => {
  var ewaLoginCookieValue;
  before(() => {
    cy.clearCookies({ domain: null });
    cy.setCookie(e.adminGuided_cookieName, e.adminGuided_cookieValue);
    cy.setCookie(e.alreadyGuided_cookieName, e.alreadyGuided_cookieValue);
  });

  it("Přechod na stránku pro přihlášení.", () => {
    cy.visit("/");
    cy.wait(2000); //spis to odstranim ???
    cy.url().should("contain", `/login`);
    cy.get(s.form, t.timeout_L).should("be.visible");
  });
  it("Kontrola ikonek pro jazyky", () => {
    cy.get(s.cs_flag, t.timeout_L).should("be.visible");
    cy.get(s.en_flag).should("be.visible");
  });
  it("Zaregistruje nového uživatele.", () => {
    cy.get(s.link_to_registration_page).click();
    cy.url().should("contain", "/registration");

    cy.contains(s.input_container, "Jméno a příjmení", t.timeout_M).within(
      () => {
        cy.get(s.flowio_text_input).type("XtestX");
      }
    );

    cy.contains(s.input_container, "Přihlašovací jméno").within(() => {
      cy.get(s.flowio_text_input).type("XtestX");
    });

    cy.contains(s.input_container, "Email").within(() => {
      cy.get(s.flowio_text_input).type("XtestX@test.com");
    });

    cy.contains(s.input_container, "Heslo").within(() => {
      cy.get(s.flowio_password_input).click().type("cypress");
    });

    cy.contains(s.input_container, "Heslo znovu").within(() => {
      cy.get(s.flowio_password_input).click().type("cypress");
    });

    cy.get(s.registration_button).click();
    cy.url().should("contain", "/login");
  });
  it("Přihlášení - FLOWIO.", () => {
    cy.login(e.getEnv().loginName, e.getEnv().loginPassword);
    cy.url().should("contain", `${Cypress.config().baseUrl}`);
    cy.get(s.modules_container, t.timeout_XL).should("be.visible");
    cy.getCookie(e.getEnv().loginCookieName)
      .its("value")
      .then((cookieValue) => {
        ewaLoginCookieValue = cookieValue;
      });
  });
  it("Smazání nového uživatele.", () => {
    cy.setCookie(e.getEnv().loginCookieName, ewaLoginCookieValue);
    cy.get(s.settings_link, t.timeout_L).should("be.visible").click();
    cy.get(s.users_link, t.timeout_M).click();
    //kliknuti na smazani konkretniho uzivatele, je zde chyba, pokud by tesne pred smazanim nekdo jeste vytvorl uzivala s jmenem XtestX1 tak by contein vybral toho uzivatele. Je osetrene pomosi should have text, ze se nespravny uzivatel nesmaze, nicmene to vyhodi chybu misto toho aby to hledalo dál meho uzivatele. zatim necham takto a v budoucnu opravím az budu vedet
    cy.contains(s.row, "XtestX", t.timeout_XL).within(() => {
      //TOHLE JE JEN PROZATIMNI RESENI - je potreba pridat atribut na tri tecky button
      cy.contains("more_horiz").click();
    });
    //.find(s.delete_button).click();
    cy.contains("ul", "Smazat").within(() => {
      cy.get(s.delete_button, t.timeout_M).click();
    });
    cy.get(s.accept_button, t.timeout_M).click();
    cy.get(s.rows, t.timeout_XL).should("be.visible");
    cy.contains("XtestX").should("not.exist");
  });
  it("Odhlášení", () => {
    cy.wait(4000);
    cy.clearCookies({ domain: null });
    cy.get(s.account_menu, t.timeout_M).click();
    cy.get(s.log_out_button, t.timeout_M).click();
    cy.reload();
    cy.visit("/");
    cy.url(t.timeout_XL).should("contain", `/login`);
  });
  it("Přighlášení GOOGLE", () => {
    cy.get(s.google_button, t.timeout_L).should("be.visible");
    cy.get(s.google_link_image)
      .invoke("attr", "src")
      .should("contain", "/static/media/google");
  });
});
