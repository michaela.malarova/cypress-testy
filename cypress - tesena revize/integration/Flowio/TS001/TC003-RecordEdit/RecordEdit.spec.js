/// <reference types="cypress" />
import * as s from "../../../../support/selectors/TS001/TC003-Editace/Editace-selectors";
import * as e from "../../../../support/envConfig/variables/envVar";
import * as t from "../../../../support/envConfig/variables/timeouts";

Cypress.Cookies.defaults({
  preserve: [
    e.getEnv().loginCookieName,
    e.adminGuided_cookieName,
    e.alreadyGuided_cookieName,
    e.getEnv().XSRF_TOKEN_DEV_CookieName,
    e.getEnv().AspNetCore_Antiforgery_CookieName,
  ],
});

context("Editace", () => {
  before(() => {
    cy.clearCookies({ domain: null }); //?:)
    cy.setCookie(e.adminGuided_cookieName, e.adminGuided_cookieValue); //-to co se me pta na tutorial adminas
    cy.setCookie(e.alreadyGuided_cookieName, e.alreadyGuided_cookieValue);
  });
  it("Přechod do FLOWIA", () => {
    //nic uz s tim proboha nedelej
    cy.visit("/");
    cy.wait(2000); //spis to odstranim ???
    cy.url().should("contain", `${Cypress.config().baseUrl}`);
    cy.get(s.modules_container, t.timeout_L).should("be.visible");
  });
  it("Přechod na šablonu.", () => {
    cy.wait(1000);
    cy.goToTemplate(e.moduleName, e.templateName);
  });
  it("CleanUp", () => {
    cy.search("Cy");
    cy.wait(1000);
    var deleteRowId = [];
    cy.get(s.row)
      .each(($el) => {
        cy.get($el).within(() => {
          cy.get(s.recordAttr_string_nazevSub)
            .invoke("text")
            .then((nazev) => {
              if (
                nazev == e.value_string_nazevSub ||
                nazev == "CyPridaniZaznamOkno" ||
                nazev == e.editedValue_string_nazevSub ||
                nazev == "CyDynDatum editovatelnost"
              ) {
                cy.get('[data-cy="subjekty5.reference_subjektu-input-column"]')
                  .invoke("text")
                  .then((value) => {
                    deleteRowId.push(value);
                  });
              }
            });
        });
      })
      .then(() => {
        if (deleteRowId.length > 0) {
          cy.get(deleteRowId).then((node) => {
            for (var i = 0; i < deleteRowId.length; i++) {
              cy.contains(s.row, node[i], t.timeout_M).within(() => {
                cy.get(s.delete_button).click();
              });
              cy.get(s.modal_content).within(() => {
                cy.get(s.accept_button).click();
              });
            }
          });
        }
      });
  });
  it("Přidat záznam v řádku + test povinné hodnoty.", () => {
    cy.get(s.add_row_record_button, t.timeout_L).click();

    //Jedno pole nevyplním
    cy.get(s.new_row, t.timeout_XL)
      .should("be.visible")
      .within(() => {
        cy.get(s.recordAttr_string_nazevSub, t.timeout_XL).should("exist");
        cy.get(s.recordAttr_int_naplanovano).type(e.value_int_naplanovano);
        cy.get(s.recordAttr_decimal_pocetDniRealita).type(
          e.value_decimal_pocetDniRealita
        ); //pak to zmen ted nejde vic jak 12 den
        cy.get(s.recordAttr_dataTime_datumPorizeni).type(
          e.value_dataTime_datumPorizeni
        );
        cy.get(s.recordAttr_string_vlastnik_Select).within(() => {
          cy.get(s.flowio_select_input).select(e.value_string_vlastnik);
        });
        cy.get(s.recordAttr_string_stav_Select).within(() => {
          cy.get(s.flowio_select_input).select(e.value_string_stav);
        });
        cy.get(s.accept_button).click();
      });
    //bez povinneho pole to nejde
    cy.get(s.new_row, t.timeout_L).should("exist");
    //doplnim povinne pole
    cy.get(s.new_row, t.timeout_S)
      .should("be.visible")
      .within(() => {
        cy.get(s.recordAttr_string_nazevSub, t.timeout_L).type(
          e.value_string_nazevSub
        );
      })
      .within(() => {
        cy.get(s.accept_button).click();
      });
    //zaznam byl vytvoren
    cy.get(s.new_row, t.timeout_XL).should("not.exist");
    cy.search(e.value_string_nazevSub);
    cy.contains(s.row, e.value_string_nazevSub, t.timeout_S).should(
      "be.visible"
    );
  });
  it("Kontrola hodnot noveho zaznamu", () => {
    cy.contains(s.row, e.value_string_nazevSub, t.timeout_S)
      .within(() => {
        cy.get(s.recordAttr_string_nazevSub, t.timeout_L).should(
          "have.text",
          e.value_string_nazevSub
        );
      })
      .within(() => {
        cy.get(s.recordAttr_int_naplanovano).should(
          "have.text",
          e.value_int_naplanovano
        );
      })
      .within(() => {
        cy.get(s.recordAttr_decimal_pocetDniRealita).should(
          "have.text",
          e.value_decimal_pocetDniRealita
        );
      })
      .within(() => {
        cy.get(s.recordAttr_dataTime_datumPorizeni).should(
          "have.text",
          e.getEnv().viewValue_dataTime_datumPorizeni
        );
      })
      .within(() => {
        cy.get(s.recordAttr_string_vlastnik_span).should(
          "have.text",
          e.viewValue_string_vlastnik
        );
      })
      .within(() => {
        cy.get(s.recordAttr_string_stav_Select).should(
          "have.text",
          e.value_string_stav
        );
      });
  });
  it("Upravit záznam v řádku.", () => {
    cy.contains(s.row, e.value_string_nazevSub).within(() => {
      cy.get(s.inline_edit_button).click();
    });
    cy.get(s.row, t.timeout_XL)
      .within(() => {
        cy.get(s.recordAttr_string_nazevSub, t.timeout_L).within(() => {
          cy.get(s.flowio_text_input)
            .clear()
            .type(e.editedValue_string_nazevSub);
        });
        cy.get(s.recordAttr_int_naplanovano, t.timeout_L).within(() => {
          cy.get(s.flowio_number_input)
            .clear()
            .type(e.editedValue_int_naplanovano);
        });
        cy.get(s.recordAttr_decimal_pocetDniRealita, t.timeout_L).within(() => {
          cy.get(s.flowio_number_input)
            .clear()
            .type(e.editedValue_decimal_pocetDniRealita);
        });
        cy.get(s.recordAttr_dataTime_datumPorizeni, t.timeout_L).within(() => {
          cy.get(s.flowio_text_input)
            .clear()
            .type(e.editedValue_dataTime_datumPorizeni);
        });
        cy.get(s.recordAttr_string_vlastnik_Select).within(() => {
          cy.get(s.flowio_select_input).select(e.editedValue_string_vlastnik);
        });
        cy.get(s.recordAttr_string_stav_Select).within(() => {
          cy.get(s.flowio_select_input).select(e.editedValue_string_stav);
        });
      })
      .within(() => {
        cy.get(s.accept_button, t.timeout_S).click();
      });
    cy.search(e.value_string_nazevSub);
    cy.contains(s.row, e.value_string_nazevSub, t.timeout_S).should(
      "not.exist"
    );
    cy.search(e.editedValue_string_nazevSub);
    cy.contains(s.row, e.editedValue_string_nazevSub, t.timeout_L).should(
      "be.visible"
    );
  });
  it("Kontrola úpravy záznam v řádku.", () => {
    cy.search(e.value_string_nazevSub);
    cy.contains(s.row, e.value_string_nazevSub, t.timeout_S).should(
      "not.exist"
    );
    cy.search(e.editedValue_string_nazevSub);
    cy.contains(s.row, e.editedValue_string_nazevSub, t.timeout_L).should(
      "be.visible"
    );
  });
  it("Odstranit záznam.", () => {
    cy.contains(s.row, e.editedValue_string_nazevSub, t.timeout_S).within(
      () => {
        cy.get(s.delete_button, t.timeout_S).click();
      }
    );
    cy.get(s.accept_button, t.timeout_M).click();
    cy.search(e.editedValue_string_nazevSub);
    cy.get(s.loader_image, t.timeout_XL).should("not.exist");
    cy.contains(s.row, e.editedValue_string_nazevSub, t.timeout_L).should(
      "not.exist"
    );
  });
  it("pridat zaznam.", () => {
    cy.get(s.add_record_button, t.timeout_M).click();
    cy.get(s.add_record_form, t.timeout_M).within(() => {
      cy.contains("tr", "string").within(() => {
        cy.get("input").type("CyPridaniZaznamOkno");
      });
      cy.contains("tr", "int").within(() => {
        cy.get("input").type(e.value_int_naplanovano);
      });
      cy.contains("tr", "decimal").within(() => {
        cy.get("input").type(e.value_decimal_pocetDniRealita);
      });
      cy.contains("tr", "dataTime").within(() => {
        cy.get("input").type(e.value_dataTime_datumPorizeni);
      });
      cy.contains("tr", "dyn.vyber").within(() => {
        cy.get(s.flowio_select_input).select(e.value_string_vlastnik);
      });
      cy.contains("tr", "stat.vyber").within(() => {
        cy.get(s.flowio_select_input).select(e.value_string_stav);
      });
    });
    //TODO
  });
  it("upravit zaznam.", () => {});
});
